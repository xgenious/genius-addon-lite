<?php
namespace Egenius\Core;
/**
 * Package Genius Addon
 * Author Xgenious
 * @since 1.0.0
 * */

if (!defined('ABSPATH')){
	exit(); //exit if access directly
}

class Admin_Menu{
	//$instance variable
	private static $instance;

	public function __construct() {
        add_action('admin_menu',array($this,'admin_menu_page'));
	}
	/**
	 * get Instance
	 * @since 1.0.0
	 * */
	public static function getInstance(){
		if (null == self::$instance){
			self::$instance = new self();
		}
		return self::$instance;
	}
    /**
     * add Menu page
     * @since 1.0.0
     * */
    public function admin_menu_page(){
        add_menu_page(
            esc_html__('Genius Addons','genius-addon-lite'),
            esc_html__('Genius Addons','genius-addon-lite'),
            'manage_options',
            'genius-addons-options',
            array($this,'genius_addons_menu_page_display'),
            GENIUS_ADDON_ADMIN_ASSETS .'/icon/menu-icon.png',
            90
        );
    }
    /**
     * admin menu page display
     * @since 1.0.0
     * */
    public function genius_addons_menu_page_display(){
        //require plugin settings page markup
        require_once GENIUS_ADDON_ADMIN .'/partials/genius-manu-page-display.php';
    }

}//end class

Admin_Menu::getInstance();

