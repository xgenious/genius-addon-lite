;(function ($) {
    "use strict";
    //GeniusAddons is a Global Variable

    var geniusTabInit = $('#genius-tabs');
    if (geniusTabInit.length) {
        geniusTabInit.tabs({
            active: 0
        });
    }
    $(document).ready(function () {

        /* --------------------------------
        *   Filter Widgets
        * ------------------------------- */

        $('.ega-addon-filter li').on('click', function (e) {
            e.preventDefault();
            var el = $(this);
            var filter = el.data('filter');
            var contentWarpper = el.parent().parent().parent().parent();
            var formArea = contentWarpper.find('.ega-form-area-warp');
            el.addClass('active').siblings().removeClass('active');
            switch (filter) {
                case "free":
                    formArea.find('.ega-widget-box.ega-pro,.ega-widget-box.ega-coming-soon').hide(500);
                    formArea.find('.ega-widget-box').not('.ega-coming-soon,.ega-pro').show(500);
                    break;
                case "pro":
                    formArea.find('.ega-widget-box').not('.ega-pro').hide(500);
                    formArea.find('.ega-widget-box.ega-pro').show(500);
                    break;
                case "coming":
                    formArea.find('.ega-widget-box').not('.ega-coming-soon').hide(500);
                    formArea.find('.ega-widget-box.ega-coming-soon').show(500);
                    break;
                default:
                    formArea.find('.ega-widget-box').show(500);
                    break;
            }
        });

        /* --------------------------------
        *   Enable All Widgets
        * ------------------------------- */
        $('.ega-enable').on('click', function (e) {
            e.preventDefault();
            var el = $(this);
            var contentWarpper = el.parent().parent().parent();
            var formArea = contentWarpper.find('.ega-form-area-warp');
            formArea.find('.ega-widget-box').not('.ega-coming-soon,.ega-pro').find('input[type="checkbox"]').attr('checked', true);
        });
        /* --------------------------------
        *   Disable All Widgets
        * ------------------------------- */
        $('.ega-disable').on('click', function (e) {
            e.preventDefault();
            var el = $(this);
            var contentWarpper = el.parent().parent().parent();
            var formArea = contentWarpper.find('.ega-form-area-warp');
            formArea.find('.ega-widget-box').not('.ega-coming-soon,.ega-pro').find('input[type="checkbox"]').attr('checked', false);
        });
        /* --------------------------------
        *   Save Change
        * ------------------------------- */
        $('.ega-save-change').on('click', function (e) {
            e.preventDefault();
            var el = $(this);
            var contentWarpper = el.parent().parent().parent();
            var widgetType = contentWarpper.attr('id').substr(4);
            var formArea = contentWarpper.find('.ega-form-area-warp');
            var allEnabledWidgetsVal = [];
            var allEnabledWidgets = formArea.find('.ega-widget-box').not('.ega-coming-soon,.ega-pro').find('input[type="checkbox"]:checked');
            allEnabledWidgets.each(function (index, value) {
                allEnabledWidgetsVal.push($(this).val())
            });
            $.ajax({
                type: "POST",
                url: GeniusAddons.ajaxUrl,
                data: {
                    action: 'genius_active_widgets',
                    nonce: GeniusAddons.nonce,
                    widget_type: widgetType,
                    active_widgets: allEnabledWidgetsVal
                },
                beforeSend:function () {
                    el.text('Saving...')
                },
                success:function (data) {
                    el.text('Save Change');
                }
            });
        });
    })
}(jQuery));