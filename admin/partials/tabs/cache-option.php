<div class="ega-tab-content" id="ega-cache" style="display: none;">
    <div class="ega-header-wrap">
        <div class="ega-left-part">
            <h4 class="ega-title"><?php esc_html_e('Cache Option','genius-addon-lite');?></h4>
            <p><?php esc_html_e('you can setup cache option','genius-addon-lite');?></p>
        </div>
        <div class="ega-right-part">
            <button type="button" class="ega-save-change"><?php esc_html_e('Save Change','genius-addon-lite');?></button>
        </div>
    </div>
</div>