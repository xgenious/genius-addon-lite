<?php
    /**
     * Template: Elements
     * @sicne 1.0.0
     * */
    $get_all_widget = Genius\Core\Widget_Manager::get_all_widgets();
    ksort($get_all_widget);
?>

<div class="ega-tab-content" id="ega-elements" style="display: none;">
	<div class="ega-header-wrap">
      <div class="ega-left-part">
          <h4 class="ega-title"><?php esc_html_e('Genius Addons','genius-addon-lite');?></h4>
          <p><?php esc_html_e('Here all of available widget list. you can enable/disable any widget from here','genius-addon-lite');?></p>
          <ul class="ega-addon-filter">
              <li class="active" data-filter="*"><?php esc_html_e('All','genius-addon-lite'); ?></li>
              <li data-filter="free"><?php esc_html_e('Free','genius-addon-lite'); ?></li>
              <li data-filter="pro"><?php esc_html_e('Pro','genius-addon-lite'); ?></li>
              <li data-filter="coming"><?php esc_html_e('Coming Soon','genius-addon-lite'); ?></li>
          </ul>
      </div>
        <div class="ega-right-part">
            <button type="button" class="ega-enable"><?php esc_html_e('Enable All','genius-addon-lite');?></button>
            <button type="button" class="ega-disable"><?php esc_html_e('Disable All','genius-addon-lite');?></button>
            <button type="button" class="ega-save-change"><?php esc_html_e('Save Change','genius-addon-lite');?></button>
        </div>
    </div>
    <div class="ega-form-area-warp">
        <?php
            foreach ($get_all_widget as $widget => $arg):
                $extra_fields = '';
                $fields_attributes = '';
	            $genius_class = '';
	            $all_active_widgets = \Genius\Core\Widget_Manager::get_active_widgets('elements') ? \Genius\Core\Widget_Manager::get_active_widgets('elements') : [];
                $extra_fields .= isset($arg['coming_soon']) && $arg['coming_soon'] ? '<span class="ega-coming-soon">'.esc_html__('Coming Soon','genius-addon-lite').'</span>' : '';
                $extra_fields .= isset($arg['is_pro_widget']) && $arg['is_pro_widget'] ? '<span class="ega-pro-badge">'.esc_html__('Pro','genius-addon-lite').'</span>' : '';
                $fields_attributes .= isset($arg['is_pro_widget']) && $arg['is_pro_widget'] || isset($arg['coming_soon']) && $arg['coming_soon']  ? ' disabled ' : '';
                $fields_attributes .= isset($arg['is_pro_widget']) && $arg['is_pro_widget'] || isset($arg['coming_soon']) && $arg['coming_soon']  ? '' : ' name="ela_widgets[]" ';
                $genius_class .= isset($arg['is_pro_widget']) && $arg['is_pro_widget'] ? 'ega-pro': '';
                $genius_class .= isset($arg['coming_soon']) && $arg['coming_soon'] ? 'ega-coming-soon': '';
	            $fields_attributes .= in_array($widget,$all_active_widgets) ? ' checked ' : '';
        ?>
        <div class="ega-widget-box <?php echo esc_attr($genius_class);?>">
            <?php echo wp_kses($extra_fields,genius_allowed_html_tags('all'))?>

            <div class="ega-label">
                <label for="ebl-widget-item-<?php echo esc_attr($widget)?>"><?php echo esc_html($arg['title']);?></label>
            </div>
            <div class="ega-checkbox">
                <label>
                    <input id="ebl-widget-item-<?php echo esc_attr($widget)?>" value="<?php echo esc_attr($widget)?>"  type="checkbox" <?php echo esc_attr($fields_attributes);?>  class="ega-switch-control">
                    <span class="ega-switch-label"></span>
                </label>
            </div>
        </div>
        <?php endforeach;?>
    </div>
</div>