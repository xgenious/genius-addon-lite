<div class="ega-tab-content" id="ega-configuration" style="display: none;">
    <div class="ega-header-wrap">
        <div class="ega-left-part">
            <h4 class="ega-title"><?php esc_html_e('Configuration','genius-addon-lite');?></h4>
            <p><?php esc_html_e('you can add here you custom css','genius-addon-lite');?></p>
        </div>
        <div class="ega-right-part">
            <button type="button" class="ega-save-change"><?php esc_html_e('Save Change','genius-addon-lite');?></button>
        </div>
    </div>
</div>