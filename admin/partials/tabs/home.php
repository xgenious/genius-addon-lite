<div class="ega-tab-content" id="ega-home">
    <div class="ega-content-area-wrap">
        <div class="ega-banner">
            <div class="thumb">
                <img src="<?php echo GENIUS_ADDON_ADMIN_ASSETS.'/image/banner.png'?>" alt="">
            </div>
        </div>
        <div class="xg-body-content">
            <div class="xg-row">
                <div class="xg-col-lg-6">
                    <div class="xg-doc-single-item">
                        <div class="thumb">
                            <img src="<?php echo GENIUS_ADDON_ADMIN_ASSETS .'/image/doc.png'?>" alt="">
                        </div>
                        <div class="content">
                            <div class="xg-admin-right-content--heading">
                                <h2 class="title"><?php esc_html_e('Easy Documentation','genius-addon-lite');?></h2>
                            </div>
                            <p><?php esc_html_e('Get started by spending some time with the documentation to get familiar with ElementsKit
                                Lite. Build awesome websites for you or your clients with ease.','genius-addon-lite');?></p>
                            <div class="btn-wrapper">
                                <a target="_blank" href="https://geniusaddon.com/docs/installation/"
                                   class="btn-more"><i class="fa fa-newspaper-o"></i><?php esc_html_e('Get
                                    started','genius-addon-lite');?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="xg-col-lg-6">
                    <div class="xg-doc-single-item">
                        <div class="thumb">
                            <img src="<?php echo GENIUS_ADDON_ADMIN_ASSETS .'/image/help.png'?>" alt="">
                        </div>
                        <div class="content">
                            <h3 class="title"><?php esc_html_e('Help and support','genius-addon-lite');?></h3>
                            <p><?php esc_html_e('Facing any technical issue? Need consultation with an expert? Simply take our live chat
                                support option.','genius-addon-lite');?></p>
                            <a class="btn-more" target="_blank" href="https://geniusaddon.com/contact/"><?php esc_html_e('Get
                                Support','genius-addon-lite');?></a>
                        </div>
                    </div>
                </div>
                <div class="xg-col xg-col-lg-6">
                    <div class="xg-doc-single-item">
                        <div class="thumb">
                            <img src="<?php echo GENIUS_ADDON_ADMIN_ASSETS .'/image/subscribe.png'?>" alt="">
                        </div>
                        <div class="content">
                            <h3 class="title"><?php esc_html_e('Newsletter Subscription','genius-addon-lite');?></h3>
                            <p><?php esc_html_e('To get updated news, current offers, deals, and tips please subscribe to our Newsletters..','genius-addon-lite');?></p>
                            <a class="btn-more" target="_blank" href=""><?php esc_html_e('Subscribe Now','genius-addon-lite');?></a>
                        </div>
                    </div>
                </div>
                <div class="xg-col xg-col-lg-6">
                    <div class="xg-doc-single-item">
                        <div class="thumb">
                            <img src="<?php echo GENIUS_ADDON_ADMIN_ASSETS .'/image/love.png'?>" alt="">
                        </div>
                        <div class="content">
                            <h4 class="title"><?php esc_html_e('Show your Love','genius-addon-lite');?></h4>
                            <p><?php esc_html_e('We love to have you in Essential Addons family. We are making it more awesome everyday.
                                Take your 2 minutes to review the plugin and spread the love to encourage us to keep it
                                going.','genius-addon-lite');?></p>

                            <a href="#"
                               class="btn-more"
                               target="_blank"><?php esc_html_e('Leave a Review','genius-addon-lite');?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>