<?php
/**
 * Template: Dashboard Page
 * @since 1.0.0
 * */
$get_all_tabs = \Genius\Core\Widget_Manager::get_all_tabs();

?>
<div class="ega-wrapper">
    <div class="ega-body" id="genius-tabs">
        <ul class="ega-tab-menu">
			<?php
                foreach ( $get_all_tabs as $tab => $arg ) {
                    if (isset($arg['hide']) && $arg['hide']) {
                        continue;
                    }
                    printf('<li style="display: inline-block;"><a href="#ega-%1$s">%2$s</a></li>',esc_attr($tab),esc_html($arg['title']));
                }
			?>
        </ul>
		<?php
		foreach ( $get_all_tabs as $tab => $arg ):
			if (isset($arg['hide']) && $arg['hide']) {
				continue;
			}
			if ( file_exists( GENIUS_ADDON_ADMIN . '/partials/tabs/' . $tab.'.php' ) ) {
				require_once GENIUS_ADDON_ADMIN . '/partials/tabs/' . $tab.'.php';
			}
		endforeach;
		?>
    </div>
</div>