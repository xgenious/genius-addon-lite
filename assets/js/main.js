;(function ($) {

    "use strict";
    /*---------------------------------------------------
      * Initialize all widget js in elementor init hook
      ---------------------------------------------------*/
    $(window).on('elementor/frontend/init', function () {
        // Brand Slider
        elementorFrontend.hooks.addAction('frontend/element_ready/genius-logo-one-widget.default', function ($scope) {
            activeBrandSlider($scope);
        });
        // Team Slider
        elementorFrontend.hooks.addAction('frontend/element_ready/genius-team-slider-widget.default', function ($scope) {
            activeTeamMemberSliderOne($scope);
        });

    });

    /*----------------------------------
        Brand Slider Widget
    --------------------------------*/
    function activeBrandSlider($scope) {
        var el = $scope.find('.brands-carousel')
        var elSettings = el.data('settings');
        if ((el.children('div').length < 2) || (elSettings.items === '0' || elSettings.items === '' || typeof elSettings.items == 'undefined')) {
            return;
        }
        let $selector = '#' + el.attr('id');

        let sliderSettings = {
            infinite: elSettings.loop === 'yes',
            slidesToShow: elSettings.items,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            autoplaySpeed: elSettings.autoplaytimeout,
            autoplay: elSettings.autoplay === 'yes',
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        }
        SlickInit($selector, sliderSettings);
    }

    /*----------------------------
          Team Member Slider
     * --------------------------*/
    function activeTeamMemberSliderOne($scope) {
        var el = $scope.find('.team-member-carousel');
        var elSettings = el.data('settings');
        if ((el.children('div').length < 2) || (elSettings.items === '0' || elSettings.items === '' || typeof elSettings.items == 'undefined')) {
            return
        }

        let $selector = '#' + el.attr('id');

        let sliderSettings = {
            infinite: elSettings.loop === 'yes',
            slidesToShow: elSettings.items,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            autoplaySpeed: elSettings.autoplaytimeout,
            autoplay: elSettings.autoplay === 'yes',
            centerMode: elSettings.center === 'yes',
            centerPadding: '0',
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        }
        SlickInit($selector, sliderSettings);
    }

    //slick init function
    function SlickInit($selector, settings, animateOut = false) {
        $($selector).slick(settings);
    }


    $(document).on('click', '.navbar-toggler', function (e) {
        e.preventDefault();
        $(".navbar-collapse.collapse").toggleClass("show");
        return false;
    })

    // Ega Accordion

    var xgaAccordion = document.getElementsByClassName('xga-accrodion');
    for (var i = 0; i < xgaAccordion.length; i++) {
        var heading = xgaAccordion[i].children[0].children[0];
        heading.onclick = function(ev){
            ev.preventDefault();
            var el = this;
            var content =  this.parentElement.nextElementSibling;
            var contentCLassLissst =  this.parentElement.classList;
            var accParentClass =  this.parentElement.parentElement.getAttribute('class');
            var groupId = document.getElementById(content.getAttribute('data-parent')).getAttribute('id');
            if (content.style.maxHeight && el.classList.contains('open')) {
                //have close accordion
                content.style.maxHeight = null;
                content.classList = 'xga-accordion-body';
            }else{
                //have to open accordion
                var ddd = document.getElementById(groupId)
                var allPrents = ddd.getElementsByClassName(accParentClass);

                // console.log();
                var elclass = el.getAttribute('class');



                for (var i = 0; i < allPrents.length; i++) {
                    allPrents[i].children[1].style.maxHeight = null;
                    allPrents[i].children[0].children[0].classList.remove('open');
                    allPrents[i].children[1].classList.remove('show');
                    allPrents[i].children[0].children[0].setAttribute('aria-expanded','false');
                }

                if (  'xga-collapse open' == elclass ) {
                    el.setAttribute('aria-expanded','true');

                }
                content.style.maxHeight = content.scrollHeight + 'px';
                content.classList = 'xga-accordion-body show';
            }
            if (el.getAttribute('aria-expanded') == 'true' ) {
                content.style.maxHeight = null;
                content.classList = 'xga-accordion-body';
                el.setAttribute('aria-expanded','false');
                el.classList.remove('open');

            }else{
                this.classList.toggle('open');
                el.setAttribute('aria-expanded','true');
            }

        }
    }

    document.addEventListener("DOMContentLoaded", function(event) {
        // Your code to run since DOM is loaded and ready

        /*---------------------------------------
        *   xgenious tab activation
        * -------------------------------------*/
        var xgtabnav = document.getElementsByClassName('xgnav-tabs');

        for (var i = 0; i < xgtabnav.length; i++) {
            var tabNav = xgtabnav[i].children;
            for (var j = 0; j < tabNav.length; j++) {

                var Nav = tabNav[j];

                Nav.onclick = function (e) {
                    var sibling = this.parentNode.children;
                    for (var s = 0; s < sibling.length; s++) {
                        sibling[s].classList.remove('active');
                    }
                    this.classList.add('active');
                    var contentID = this.attributes['data-target'].value.substr(1);
                    var tabPaneId = this.parentNode.nextElementSibling.children;
                    for (var t = 0; t < tabPaneId.length; t++) {
                        tabPaneId[t].classList.remove('active');
                        tabPaneId[t].classList = 'xgtab-pane fade';
                    }
                    document.getElementById(contentID).classList.add('active');
                    document.getElementById(contentID).classList.add('show');
                };

            };

        }

    });


})(jQuery);