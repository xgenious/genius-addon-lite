<?php
namespace Egenius\Core;

if (!defined('ABSPATH')){
    exit(); //exit if access directly
}

/**
 * Genius Addon
 * @since 1.0.0
 * */

class Init{
	//$instance variable
	private static $instance;
	private static $minimum_elementor_version  = '2.5.0';
	private static $minimum_php_version = '7.0';

	public function __construct() {

	    //load plugin textdomain
        add_action('init',array($this,'i18n'));

		//check elementor active or not
		if ( ! did_action( 'elementor/loaded' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_elementor_plugin' ] );
			return;
		}

		//check elementor required version
		if ( ! version_compare( ELEMENTOR_VERSION, self::$minimum_elementor_version, '>=' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_elementor_version' ] );
			return;
		}

		//check PHP Version
		if ( version_compare( PHP_VERSION, self::$minimum_php_version, '<' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_php_version' ] );
			return;
		}

		//plugins settings page add in plugins page
		add_filter('plugin_action_links_'.GENIUS_ADDON_BASE_NAME, [ $this, 'plugins_setting_links' ] );

		//load dependency files
		self::load_dependency_files();
	}
	/**
	 * get Instance
	 * @since 1.0.0
	 * */
	public static function getInstance(){
		if (null == self::$instance){
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * load plugin textdomain
     * @since 1.0.0
	 * */
	public static function i18n(){
	    load_plugin_textdomain('genius-addon-lite',false,GENIUS_ADDON_PATH .'/languages');
    }


    /**
     * Admin notice for elementor activation
     * @since 1.0.0
     *
     * */
    public function admin_notice_elementor_plugin(){
        if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );
        $elementor = 'elementor/elementor.php';
        if( self::is_active_plugin( $elementor ) ) {
	        if( ! current_user_can( 'activate_plugins' ) ) { return; }

	        $activation_url = wp_nonce_url( 'plugins.php?action=activate&amp;plugin=' . $elementor . '&amp;plugin_status=all&amp;paged=1&amp;s', 'activate-plugin_' . $elementor );

	        $message = '<p>' .'<strong>'.esc_html__('Genius Addons','genius-addon-lite').'</strong>'. esc_html__( ' not working because you need to activate the','genius-addon-lite').'<strong>'.esc_html__(' Elementor Plugin','genius-addon-lite').'</strong>'. '</p>';
	        $message .= '<p>' . sprintf( '<a href="%s" class="button-primary">%s</a>', $activation_url, esc_html__( 'Elementor Activate Now', 'genius-addon-lite') ) . '</p>';
        } else {
	        if ( ! current_user_can( 'install_plugins' ) ) { return; }

	        $install_url = wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=elementor' ), 'install-plugin_elementor' );

	        $message = '<p>' .'<strong>'.esc_html__('Genius Addons','genius-addon-lite').'</strong>'. esc_html__( ' not working because you need to install the ', 'genius-addon-lite') .'<strong>'.esc_html__('Elementor Plugin','genius-addon-lite').'</strong>'. '</p>';

	        $message .= '<p>' . sprintf( '<a href="%s" class="button">%s</a>', $install_url, esc_html__( 'Elementor Install Now', 'genius-addon-lite') ) . '</p>';
        }
        echo  '<div class="error"><p>' . wp_kses($message,genius_allowed_html_tags('all')) . '</p></div>';
    }
    /**
     * Admin notice for minimum elementor version
     * @since 1.0.0
     *
     * */
    public function admin_notice_minimum_elementor_version(){
        if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );
        $message = sprintf(
	        __( '"%1$s" requires "%2$s" version %3$s or greater.', 'genius-addon-lite'),
	        '<strong>' . esc_html__( 'Genius Addons', 'genius-addon-lite') . '</strong>',
	        '<strong>' . esc_html__( 'Elementor', 'genius-addon-lite') . '</strong>',
	        self::$minimum_elementor_version
        );
        printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );
    }
    /**
     * Admin notice for minimum php version
     * @since 1.0.0
     *
     * */
    public function admin_notice_minimum_php_version(){
        if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );
        $message = sprintf(
	        __( '"%1$s" requires "%2$s" version %3$s or greater.', 'genius-addon-lite'),
	        '<strong>' . esc_html__( 'Genius Addons', 'genius-addon-lite') . '</strong>',
	        '<strong>' . esc_html__( 'PHP', 'genius-addon-lite') . '</strong>',
	        self::$minimum_php_version
        );
        printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );
    }


    public function plugins_setting_links($links){
        $genius_settings_link = '<a href="admin.php?page=genius-addons-options">'.esc_html__( 'Settings', 'genius-addon-lite').'</a>';
        array_unshift( $links, $genius_settings_link );
        if( !defined('GENIUS_ADDON_PRO_VERSION') ){
	        $links['ea_go_pro'] = sprintf('<a href="#" target="_blank" style="color: #c92551; font-weight: bold;">' . esc_html__('Go Pro','genius-addon-lite') . '</a>');
        }
        return $links;
    }
	public static function is_active_plugin($file_path = null){
		$installed_plugins_list = get_plugins();
		return isset( $installed_plugins_list[$file_path] );
	}

	/**
	 * load all the dependency files
     * @since 1.0.0
	 * */
	public static function load_dependency_files(){
	        $include_files = array(
		        array(
	                'file-path' => GENIUS_ADDON_INC,
	                'file-name' => 'genius-functions'
	            ),
                array(
	                'file-path' => GENIUS_ADDON_ADMIN,
	                'file-name' => 'class-genius-admin-menu'
	            ),
		        array(
	                'file-path' => GENIUS_ADDON_CLASSES,
	                'file-name' => 'class-assets-manager'
	            ),
		        array(
	                'file-path' => GENIUS_ADDON_CLASSES,
	                'file-name' => 'class-genius-icon-manager'
	            ),
		        array(
	                'file-path' => GENIUS_ADDON_CLASSES,
	                'file-name' => 'class-widget-manager'
	            ),
	        );

	        if(is_array($include_files) && !empty($include_files)){
	            foreach ($include_files as $file){
	                if (file_exists($file['file-path'] .'/'.$file['file-name'].'.php')){
	                    require_once $file['file-path'].'/'.$file['file-name'].'.php';
	                }
	            }
	        }

    }

}//end class

Init::getInstance();

