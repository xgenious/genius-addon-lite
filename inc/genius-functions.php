<?php

/**
 * Package Genius Addon
 * Author Xgenious
 * @since 1.0.0
 * */

if (!defined('ABSPATH')){
	exit(); //exit if access directly
}

/**
 * kses
 * @since 1.0.0
 * */
 function genius_allowed_html_tags( $allowed_tags = 'all' ) {
	 $allowed_html = array(
		 'div'    => ['class' => [], 'id' => [] ],
		 'header' => ['class' => [], 'id' => [] ],
		 'h1'     => ['class' => [], 'id' => [] ],
		 'h2'     => ['class' => [], 'id' => [] ],
		 'h3'     => ['class' => [], 'id' => [] ],
		 'h4'     => ['class' => [], 'id' => [] ],
		 'h5'     => ['class' => [], 'id' => [] ],
		 'h6'     => ['class' => [], 'id' => [] ],
		 'p'      => ['class' => [], 'id' => [] ],
		 'span'   => ['class' => [], 'id' => [] ],
		 'i'      => ['class' => [], 'id' => [] ],
		 'mark'   => ['class' => [], 'id' => [] ],
		 'strong' => ['class' => [], 'id' => [] ],
		 'br'     => ['class' => [], 'id' => [] ],
		 'b'      => ['class' => [], 'id' => [] ],
		 'em'     => ['class' => [], 'id' => [] ],
		 'del'    => ['class' => [], 'id' => [] ],
		 'ins'    => ['class' => [], 'id' => [] ],
		 'u'      => ['class' => [], 'id' => [] ],
		 's'      => ['class' => [], 'id' => [] ],
		 'nav'    => ['class' => [], 'id' => [] ],
		 'ul'     => ['class' => [], 'id' => [] ],
		 'li'     => ['class' => [], 'id' => [] ],
		 'form'   => ['class' => [], 'id' => [] ],
		 'select' => ['class' => [], 'id' => [] ],
		 'option' => ['class' => [], 'id' => [] ],
		 'img'    => ['class' => [], 'id' => [] ],
		 'a'      => ['class' => [], 'id' => [], 'href' => [],'rel' => []],
	 );

	 if ( 'all' == $allowed_tags ) {
		 return $allowed_html;
	 } else {
		 if ( is_array( $allowed_tags ) && ! empty( $allowed_tags ) ) {
			 $specific_tags = [];
			 foreach ( $allowed_tags as $allowed_tag ) {
				 if ( array_key_exists( $allowed_tag, $allowed_html ) ) {
					 $specific_tags[ $allowed_tag ] = $allowed_html[ $allowed_tag ];
				 }
			 }
			 return $specific_tags;
		 }
	 }
}

 function genius_is_active_plugin( $file_path ) {
	$installed_plugins_list = get_plugins();

	return isset( $installed_plugins_list[ $file_path ] );
}
 function genius_is_pro() {
	return defined('GENIUS_ADDON_PRO_VERSION') ? true : false;
}

function genius_version_compare($current_version,$required_version,$operator){
 	return  version_compare( $current_version, $required_version, $operator );
}
/**
 * Get list of nav menu
 * @since 1.0.0
 * */
function get_nav_menu_list( $output = 'slug' ) {
    $return_val    = [];
    $all_menu_list = wp_get_nav_menus();

    foreach ( $all_menu_list as $menu ) {
        if ( $output == 'slug' ) {
            $return_val[ $menu->slug ] = $menu->name;
        } else {
            $return_val[ $menu->term_id ] = $menu->name;
        }
    }

    return $return_val;
}
/**
 * render elementor link attributes
 * @since 1.0.0
 * */
function render_elementor_link_attributes($link, $class = null)
{
    $return_val = '';

    if (!empty($link['url'])) {
        $return_val .= 'href="' . esc_url($link['url']) . '"';
    }
    if (!empty($link['is_external'])) {
        $return_val .= 'target="_blank"';
    }
    if (!empty($link['nofollow'])) {
        $return_val .= 'rel="nofollow"';
    }
    if (!empty($class)) {
        if (is_array($class)) {
            $return_val .= 'class="';
            foreach ($class as $cl) {
                $return_val .= $cl . ' ';
            }
            $return_val .= '"';
        } else {
            $return_val .= 'class="' . esc_attr($class) . '"';
        }
    }

    return $return_val;
}
/*----------------------------
	CONTACT FORM 7 RETURN ARRAY
-------------------------------*/
function xga_get_contact_form_shortcode_list_el(){

    $forms_list = array();
    $forms_args = array( 'posts_per_page' => -1, 'post_type'=> 'wpcf7_contact_form' );
    $forms      = get_posts( $forms_args );

    if( $forms ){
        foreach ( $forms as $form ){
            $forms_list[$form->ID] = $form->post_title;
        }
    }else{
        $forms_list[ esc_html__( 'No contact form found', 'genius-addon-lite' ) ] = 0;
    }
    return $forms_list;
}