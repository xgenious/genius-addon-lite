<?php
namespace Genius\Core;
/**
 *
 * Package Genius Addon
 * Author Xgenious
 * @since 1.0.0
 * */

if ( ! defined( 'ABSPATH' ) ) {
	exit(); //exit if access directly
}

class Widget_Manager {
	private static $instance;

	public function __construct() {
		//handle ajax request
		add_action('wp_ajax_genius_active_widgets',[__CLASS__,'handle_ajax_request']);
		//register new elementor category
		add_action('elementor/elements/categories_registered',[__CLASS__,'category_register']);
		//register widgets
		add_action('elementor/widgets/widgets_registered',[__CLASS__,'widget_register']);
		//register editor style
		add_action('elementor/editor/after_enqueue_styles',[__CLASS__,'editor_style']);
	}

	/**
	 * get Instance
	 * @since 1.0.0
	 * */
	public static function getInstance() {
		if ( null == self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * All Free Widgets
	 * @since 1.0.0
	 * */
	public static function free_widgets() {
		return [
			'blockquote'          => [
				'title' => esc_html__( 'Blockquote', 'genius-addon-lite' )
			],
			'logo-slider'         => [
				'title' => esc_html__( 'Logo Slider', 'genius-addon-lite' )
			],
			'alert-block'         => [
				'title' => esc_html__( 'Alert', 'genius-addon-lite' )
			],
			'btn'                 => [
				'title' => esc_html__( 'Button', 'genius-addon-lite' )
			],
			'btn-group'           => [
				'title' => esc_html__( 'Button Group', 'genius-addon-lite' ),
                'coming_soon' => true
			],
			'btn-compare'         => [
				'title'       => esc_html__( 'Button Compare', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'team-slider'         => [
				'title'       => esc_html__( 'Team Slider', 'genius-addon-lite' )
			],
			'testimonial-slider'  => [
				'title'       => esc_html__( 'Testimonial Slider', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'icon-box'            => [
				'title' => esc_html__( 'Icon Box', 'genius-addon-lite' )
			],
			'price-plan'          => [
				'title'       => esc_html__( 'Price Plan', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'progressbar'         => [
				'title'       => esc_html__( 'Progressbar', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'video-popup'         => [
				'title'       => esc_html__( 'Video Popup', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'info-box'            => [
				'title' => esc_html__( 'Info Box', 'genius-addon-lite' )
			],
			'service-box'         => [
				'title' => esc_html__( 'Service Box', 'genius-addon-lite' ),
                'coming_soon' => true
			],
			'dual-color-headline' => [
				'title'       => esc_html__( 'Dual Color Headline', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'card'                => [
				'title' => esc_html__( 'Card', 'genius-addon-lite' )
			],
			'accordion'           => [
				'title'       => esc_html__( 'Accordion', 'genius-addon-lite' )
			],
            'navbar'           => [
                'title'       => esc_html__( 'Navigation', 'genius-addon-lite' )
            ],
            'tabs'           => [
                'title'       => esc_html__( 'Tabs', 'genius-addon-lite' )
            ],
			'modal-box'           => [
				'title'       => esc_html__( 'Modal Box', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'contact-widgets'     => [
				'title'       => esc_html__( 'Contact Widget', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'tooltips'            => [
				'title' => esc_html__( 'Tooltips', 'genius-addon-lite' ),
                'coming_soon' => true
			],
			'video-player'        => [
				'title'       => esc_html__( 'Video Player', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'slider'              => [
				'title' => esc_html__( 'Slider', 'genius-addon-lite' ),
                'coming_soon' => true
			],
			'social-share'        => [
				'title' => esc_html__( 'Social Share', 'genius-addon-lite' ),
                'coming_soon' => true
			],
			'image-hover'         => [
				'title' => esc_html__( 'Image Hover', 'genius-addon-lite' )
			],
			'counterup'         => [
				'title' => esc_html__( 'Counterup', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'countdown'         => [
				'title' => esc_html__( 'Coundown', 'genius-addon-lite' ),
				'coming_soon' => true
			],
		];
	}

	/**
	 * All Free Widgets
	 * @since 1.0.0
	 * */
	public static function third_party_widgets() {
		return [
			'contact-form-7'                => [
				'title' => esc_html__( 'Contact Form 7', 'genius-addon-lite' )
			],
			'woo-product-slider'            => [
				'title'       => esc_html__( 'Woo Product Slider', 'genius-addon-lite' ),
                'coming_soon' => true
			],
			'woo-product-list'              => [
				'title'       => esc_html__( 'Woo Product List', 'genius-addon-lite' ),
				'is_pro_widget' => true
			],
			'tablepress'                    => [
				'title'       => esc_html__( 'TablePress', 'genius-addon-lite' ),
				'is_pro_widget' => true
			],
			'wp-form'                       => [
				'title'       => esc_html__( 'WP Form', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'easy-digital-downloads-slider' => [
				'title'       => esc_html__( 'EDD Slider', 'genius-addon-lite' ),
                'coming_soon' => true
			],
			'easy-digital-downloads-list'   => [
				'title'       => esc_html__( 'EDD List', 'genius-addon-lite' ),
				'is_pro_widget' => true
			],
			'learnpress-list'   => [
				'title'       => esc_html__( 'LearnPress List', 'genius-addon-lite' ),
				'is_pro_widget' => true
			],
			'learnpress-slider'   => [
				'title'       => esc_html__( 'LearnPress Slider', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'budypress-list'   => [
				'title'       => esc_html__( 'BuddyPress List', 'genius-addon-lite' ),
				'is_pro_widget' => true
			],
			'buddypress-slider'   => [
				'title'       => esc_html__( 'BuddyPress Slider', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'wpforms'   => [
				'title'       => esc_html__( 'Wpforms', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'ninja-forms'   => [
				'title'       => esc_html__( 'Ninja Forms', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'gravity-forms'   => [
				'title'       => esc_html__( 'Gravity Forms', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'breadcrumb-navxt'   => [
				'title'       => esc_html__( 'Breadcrumb NavXT', 'genius-addon-lite' ),
				'coming_soon' => true
			],
			'wp-google-maps'   => [
				'title'       => esc_html__( 'WP Google Maps', 'genius-addon-lite' ),
				'is_pro_widget' => true
			]
		];
	}

	/**
	 * All Pro Widgets
	 * @since 1.0.0
	 * */
	public static function pro_widgets() {
		return [
			'advance-heading'            => [
				'title'         => esc_html__( 'Advance Heading', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'post-slider-advance'        => [
				'title'         => esc_html__( 'Post Slider Advance', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'price-plan-advance'         => [
				'title'         => esc_html__( 'Price Plan Advance', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'countdown-advanced'                  => [
				'title'         => esc_html__( 'Countdown Advanced', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'call-to-action'             => [
				'title'         => esc_html__( 'Call To Action', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'filterable-gallery'         => [
				'title'         => esc_html__( 'Filterable Gallery', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'image-compare'              => [
				'title'         => esc_html__( 'Image Compare', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'team-slider-advance'        => [
				'title'         => esc_html__( 'Advance Team Slider', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'testimonial-slider-advance' => [
				'title'         => esc_html__( 'Advance Testimonial Slider', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'circle-skill'               => [
				'title'         => esc_html__( 'Circle Skill', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'portfolio-advance'          => [
				'title'         => esc_html__( 'Portfolio', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'promo-box'                  => [
				'title'         => esc_html__( 'Promo Box', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'animated-text'              => [
				'title'         => esc_html__( 'Animated Text', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'table'                      => [
				'title'         => esc_html__( 'Table', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'data-table'                 => [
				'title'         => esc_html__( 'Data Table', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'instagram-feed'             => [
				'title'         => esc_html__( 'Instagram Feed', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'twitter-feed'               => [
				'title'         => esc_html__( 'Instagram Feed', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'facebook-feed'              => [
				'title'         => esc_html__( 'Instagram Feed', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'image-hover-advance'        => [
				'title'         => esc_html__( 'Image Hover Advance', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'mailchimp-form'        => [
				'title'         => esc_html__( 'Mailchimp', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'login-form'        => [
				'title'         => esc_html__( 'Login Form', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'register-form'        => [
				'title'         => esc_html__( 'Register Form', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'bar-chart'        => [
				'title'         => esc_html__( 'Bar Chart', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'line-chart'        => [
				'title'         => esc_html__( 'Line Chart', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'area-chart'        => [
				'title'         => esc_html__( 'Area Chart', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'pie-chart'        => [
				'title'         => esc_html__( 'Pie Chart', 'genius-addon-lite' ),
				'is_pro_widget' => true,
			],
			'counterup-advance'         => [
				'title' => esc_html__( 'Counterup Advanced', 'genius-addon-lite' ),
				'is_pro_widget' => true
			],
		];
	}

	/**
	 * All Widgets ( except third party addons )
	 * @since 1.0.0
	 * */
	public static function get_all_widgets() {
		return array_merge(self::free_widgets(),self::pro_widgets());
	}

	/**
	 * All Widgets ( except third party addons )
	 * @since 1.0.0
	 * */
	public static function get_all_tabs() {
		return [
			'home' => [
				'title' => esc_html__( 'Home', 'genius-addon-lite' ),
			],
			'elements' => [
				'title' => esc_html__( 'Elements', 'genius-addon-lite' ),
			],
			'third-party' => [
				'title' => esc_html__( 'Third Party', 'genius-addon-lite' ),
			],
			'extensions' => [
				'title' => esc_html__( 'Extensions', 'genius-addon-lite' ),
				'hide'  => true,
			],
			'configurations' => [
				'title' => esc_html__( 'Configurations', 'genius-addon-lite' ),
				'hide'  => true,
			],
			'custom-css' => [
				'title' => esc_html__( 'Custom Css', 'genius-addon-lite' ),
				'hide' => true
			],
			'cache-option' => [
				'title' => esc_html__( 'Cache Option', 'genius-addon-lite' ),
				'hide'  => true,
			],
			'get-pro' => [
				'title' => esc_html__( 'Get Pro', 'genius-addon-lite' ),
                'hide'  => true,
			],
		];
	}

	/**
	 * widget_option_key
	 * @since 1.0.0
	 * */
	public static function widget_option_key($widget_type){
		$widget_type = str_replace('-','_',$widget_type);
		return 'genius_'.$widget_type.'_active_widgets';
	}

	/**
	 * update_widget_option
	 * @since 1.0.0
	 * */
	public static function update_widget_option($widget_type,$value){
		update_option(self::widget_option_key($widget_type),$value);
	}
	/**
	 * update_widget_option
	 * @since 1.0.0
	 * */
	public  static  function get_active_widgets($widget_type){
		return unserialize(get_option(self::widget_option_key($widget_type)));
	}

	/**
	 * handle ajax request
	 * @since 1.0.0
	 * */
	public function handle_ajax_request(){
		if (!current_user_can('manage_options')){
			wp_die();
		}
		if (!wp_verify_nonce($_POST['nonce'],'genius_dashboard_nonce')){
			wp_die();
		}
		$data = isset($_POST['active_widgets']) && !empty($_POST['active_widgets']) ? sanitize_text_field( $_POST['active_widgets']) : '';
		$widget_type = isset($_POST['widget_type']) && !empty($_POST['widget_type']) ? sanitize_text_field($_POST['widget_type']) : '';
		self::update_widget_option($widget_type,serialize($data));
		wp_die();
	}

	/**
	 * Elementor Category Register
	 * @since 1.0.0
	 * */
	public static function category_register($element_manager){
		$element_manager->add_category(
			'ega-widgets',
			[
				'title' => esc_html__('Genius Addons','genius-addon-lite'),
				'icon' => 'eicon-folder-o'
			]
		);
	}

	/**
	 * Elementor Widget Register
	 * @since 1.0.0
	 * */
	public static function widget_register(){
		$all_element_widgets = self::get_active_widgets('elements') ? self::get_active_widgets('elements') : [];
		$all_third_party_widgets = self::get_active_widgets('third-party') ? self::get_active_widgets('third-party') : [];
		$all_active_widgets = array_merge($all_element_widgets,$all_third_party_widgets);
		if (genius_is_pro() && defined('GENIUS_ADDON_PRO_VERSION')){
			$all_active_widgets = array_merge($all_active_widgets,self::pro_widgets());
		}
		foreach ($all_active_widgets as $widget){
			self::include_widget_file($widget);
		}
	}
	/**
	 * Elementor Widget Register
	 * @since 1.0.0
	 * */
	public static function include_widget_file($widget_slug){
		if(is_readable(GENIUS_ADDON_WIDGETS.'/'.$widget_slug.'/widget.php')){
			require_once GENIUS_ADDON_WIDGETS.'/'.$widget_slug.'/widget.php';
		}
	}

	/**
	 * Elementor Editor Style
	 * @since 1.0.0
	 * */
	public static function editor_style(){
		wp_enqueue_style('genius-editor-style',GENIUS_ADDON_ASSETS.'/css/genius-editor.css',[],GENIUS_ADDON_VERSION,'all');
	}

}//end class

Widget_Manager::getInstance();