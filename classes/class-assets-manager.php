<?php

namespace Genius\Core;
/**
 * Package Genius Addon
 * Author Xgenious
 * @since 1.0.0
 * */

if (!defined('ABSPATH')) {
    exit(); //exit if access directly
}

class Assets_Manager
{

    private static $instance;

    public function __construct()
    {

        //load plugin assets
        add_action('wp_enqueue_scripts', array($this, 'plugins_assets'));

        //load admin assets
        add_action('admin_enqueue_scripts', array($this, 'admin_assets'));

        //load global variable
        add_action('admin_head', array($this, 'load_global_script'));

        //add icon to elementor new icons fileds
        add_filter('elementor/icons_manager/native', array($this, 'add_custom_icon_to_elementor_icons'));
    }

    /**
     * get Instance
     * @since 1.0.0
     * */
    public static function getInstance()
    {
        if (null == self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * plugins assets
     * @sicne 1.0.0
     * */
    public function plugins_assets()
    {
        self::enqueue_css();
        self::enqueue_js();
    }

    /**
     * enqueue_css
     * @since 1.0.0
     * */
    public function enqueue_css()
    {
        $include_css = array(
            array(
                'handle' => 'line-awesome',
                'src' => GENIUS_ADDON_ASSETS . '/css/line-awesome.css',
                'ver' => GENIUS_ADDON_VERSION,
                'deps' => array(),
                'media' => 'all',
            ),
            array(
                'handle' => 'xg-accordion',
                'src' => GENIUS_ADDON_ASSETS . '/css/xg-accordion.css',
                'ver' => GENIUS_ADDON_VERSION,
                'deps' => array(),
                'media' => 'all',
            ),
            array(
                'handle' => 'genius-tabs',
                'src' => GENIUS_ADDON_ASSETS . '/css/genius-tabs.css',
                'ver' => GENIUS_ADDON_VERSION,
                'deps' => array(),
                'media' => 'all',
            ),
            array(
                'handle' => 'ega-main-style',
                'src' => GENIUS_ADDON_ASSETS . '/css/genius-main-style.css',
                'ver' => GENIUS_ADDON_VERSION,
                'deps' => array(),
                'media' => 'all',
            ),
        );
        $include_css = apply_filters('genius_addons_frontend_stylesheets', $include_css);
        if (is_array($include_css) && !empty($include_css)) {
            foreach ($include_css as $css) {
                call_user_func_array('wp_enqueue_style', $css);
            }
        }
    }

    /**
     * enqueue_js
     * @since 1.0.0
     * */
    public function enqueue_js()
    {


        $include_js = array(
            array(
                'handle' => 'lineawesome',
                'src' => GENIUS_ADDON_ASSETS . '/js/lineawesome.js',
                'deps' => array('jquery'),
                'ver' => GENIUS_ADDON_VERSION,
                'in_footer' => true
            ),
            array(
                'handle' => 'lineawesome-las',
                'src' => GENIUS_ADDON_ASSETS . '/js/lineawesome.las.js',
                'deps' => array('jquery'),
                'ver' => GENIUS_ADDON_VERSION,
                'in_footer' => true
            ),
            array(
                'handle' => 'slick',
                'src' => GENIUS_ADDON_ASSETS . '/js/slick.min.js',
                'deps' => array('jquery'),
                'ver' => GENIUS_ADDON_VERSION,
                'in_footer' => true
            ),
            array(
                'handle' => 'genius-master-main-script',
                'src' => GENIUS_ADDON_ASSETS . '/js/main.js',
                'deps' => array('jquery'),
                'ver' => GENIUS_ADDON_VERSION,
                'in_footer' => true
            ),
        );

        if (is_array($include_js) && !empty($include_js)) {
            foreach ($include_js as $js) {
                call_user_func_array('wp_enqueue_script', $js);
            }
        }
    }


    /**
     *  admin assets
     * @sicne 1.0.0
     * */
    public function admin_assets()
    {
        self::enqueue_admin_css();
        self::enqueue_admin_js();
    }

    /**
     * enqueue_css
     * @since 1.0.0
     * */
    public function enqueue_admin_css()
    {
        //load deafult wordpres codemirror style
        wp_enqueue_style('wp-codemirror');
        $include_css = array(
            array(
                'handle' => 'ea-admin-style',
                'src' => GENIUS_ADDON_ADMIN_ASSETS . '/css/ea-admin.css',
                'ver' => GENIUS_ADDON_VERSION,
                'deps' => array(),
                'media' => 'all',
            ),
        );

        if (is_array($include_css) && !empty($include_css)) {
            foreach ($include_css as $css) {
                call_user_func_array('wp_enqueue_style', $css);
            }
        }
    }

    /**
     * enqueue_js
     * @since 1.0.0
     * */
    public function enqueue_admin_js()
    {
        //load default scripts
        wp_enqueue_script('jquery-ui-tabs');

        //load plugins script
        $include_js = array(
            array(
                'handle' => 'genius-dashboard',
                'src' => GENIUS_ADDON_ADMIN_ASSETS . '/js/dashboard.js',
                'ver' => GENIUS_ADDON_VERSION,
                'deps' => array('jquery', 'jquery-ui-tabs'),
                'in_footer' => true
            ),
        );

        if (is_array($include_js) && !empty($include_js)) {
            foreach ($include_js as $js) {
                call_user_func_array('wp_enqueue_script', $js);
            }
        }

    }

    public function add_custom_icon_to_elementor_icons($icons)
    {
        $icons['lineawesome'] = [
            'name' => 'lineawesome',
            'label' => esc_html__('Brand Icon (Line Awesome)', 'genius-addon-lite'),
            'url' => GENIUS_ADDON_ASSETS . '/css/line-awesome.css',
            // icon css file
            'enqueue' => [GENIUS_ADDON_ASSETS . '/css/line-awesome.css'],
            // icon css file
            'prefix' => 'la-',
            //prefix ( like fas-fa  )
            'displayPrefix' => 'lab',
            //prefix to display icon
            'labelIcon' => 'lab la-accessible-icon',
            //tab icon of elementor icons library
            'ver' => '1.0.0',
            'fetchJson' => GENIUS_ADDON_ASSETS . '/js/lineawesome.js',
            //json file with icon list example {"icons: ['icon class']}
            'native' => true,
        ];
        $icons['lineawesome_las'] = [
            'name' => 'lineawesome_las',
            'label' => esc_html__('Free Icon (Line Awesome)', 'genius-addon-lite'),
            'url' => GENIUS_ADDON_ASSETS . '/css/line-awesome.css',
            // icon css file
            'enqueue' => [GENIUS_ADDON_ASSETS . '/css/line-awesome.css'],
            // icon css file
            'prefix' => 'la-',
            //prefix ( like fas-fa  )
            'displayPrefix' => 'las',
            //prefix to display icon
            'labelIcon' => 'lab la-accessible-icon',
            //tab icon of elementor icons library
            'ver' => '1.0.0',
            'fetchJson' => GENIUS_ADDON_ASSETS . '/js/lineawesome.las.js',
            //json file with icon list example {"icons: ['icon class']}
            'native' => true,
        ];

        return $icons;
    }

    /**
     * laod global variable
     * @since 1.0.0
     *
     * */
    public function load_global_script()
    {
        $ajax_url = json_encode(admin_url("admin-ajax.php"));
        $admin_page_slug = '';
        $genius_nonce = json_encode(wp_create_nonce('genius_dashboard_nonce'));
        echo <<<JS
	 <script>
	 	var  GeniusAddons = {
    		"ajaxUrl" : {$ajax_url},
    		"nonce" : {$genius_nonce},
    		"adminPageSlug" : "genius-addons-options"
	 	}
	  </script>
JS;

    }

}//end class

Assets_Manager::getInstance();