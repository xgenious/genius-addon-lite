<?php
if (!defined('ABSPATH')){
    exit(); //exit if access it directly
}
/**
 * Plugin Name: Genius Addon - A Plugin of Elementor Addon & Template Library Collection
 * Description: Genius Addon is a light wet elementor widgets and template library collection.
 * Plugin URI: https://geniusaddon.com/
 * Author: xgenious
 * Version: 1.0.0
 * Author URI: https://xgenious.com/
 * Requires at least: 5.3
 * Tested up to: 5.8.1
 * Requires PHP: 7.1
 * Text Domain: genius-addon-lite
 *
 * @package Genius Addon
 * @category Free
 *
 * Genius Addon is a light wet elementor widgets and template library collection.
 *
 */

/**
 * Define Plugin Dir path
 * @since 1.0.0
 * */
define('GENIUS_ADDON_PATH',plugin_dir_path(__FILE__));
define('GENIUS_ADDON_URL',plugin_dir_url(__FILE__));
define('GENIUS_ADDON_DEV_MODE',true);
define('GENIUS_ADDON_BASE_NAME',plugin_basename(__FILE__));
define('GENIUS_ADDON_INC',GENIUS_ADDON_PATH .'/inc');
define('GENIUS_ADDON_CLASSES',GENIUS_ADDON_PATH .'/classes');
define('GENIUS_ADDON_ASSETS',GENIUS_ADDON_URL .'assets');
define('GENIUS_ADDON_ADMIN',GENIUS_ADDON_PATH .'/admin');
define('GENIUS_ADDON_ADMIN_ASSETS',GENIUS_ADDON_URL .'/admin/assets');
define('GENIUS_ADDON_WIDGETS',GENIUS_ADDON_PATH .'/widgets');

if (GENIUS_ADDON_DEV_MODE){
	define('GENIUS_ADDON_VERSION','1.0.0');
}else{
	define('GENIUS_ADDON_VERSION',time());
}
/**
 * plugin activation
 * @since 1.0.0
 * */
function genius_plugin_activate(){
	if (file_exists(GENIUS_ADDON_INC .'/class-genius-addon-init.php')){
		require_once GENIUS_ADDON_INC .'/class-genius-addon-init.php';
	}
}

add_action('plugins_loaded','genius_plugin_activate');