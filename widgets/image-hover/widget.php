<?php
namespace Elementor;
/**
 * Package genius Addon
 * Author Xgenious
 * @since 1.0.0
 * */

if (!defined('ABSPATH')){
	exit(); //exit if access directly
}
class Genius_Image_Hover extends Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Elementor widget name.
	 *
	 * @return string Widget name.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_name() {
		return 'genius-image-hover-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Elementor widget title.
	 *
	 * @return string Widget title.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_title() {
		return esc_html__( 'Image Hover', 'genius-addon-lite' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Elementor widget icon.
	 *
	 * @return string Widget icon.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_icon() {
		return 'eicon-image xga';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Elementor widget belongs to.
	 *
	 * @return array Widget categories.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_categories() {
		return [ 'ega-widgets' ];
	}

	/**
	 * Register Elementor widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'settings_section',
			[
				'label' => esc_html__( 'General Settings', 'genius-addon-lite' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);
		$this->add_control( 'active_widget', [
			'label' => esc_html__( 'Make It Active', 'genius-addon-lite' ),
			'type'  => Controls_Manager::SWITCHER,
		] );
		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'genius-addon-lite' ),
				'type'        => Controls_Manager::TEXTAREA,
				'label_block' => true,
				'description' => esc_html__( 'enter title.', 'genius-addon-lite' ),
				'default'     => esc_html__( 'Be A Volunteer', 'genius-addon-lite' )
			]
		);
		$this->add_control(
			'description',
			[
				'label'       => esc_html__( 'Description', 'genius-addon-lite' ),
				'type'        => Controls_Manager::TEXTAREA,
				'label_block' => true,
				'description' => esc_html__( 'enter description.', 'genius-addon-lite' ),
				'default'     => esc_html__( 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim Nullam in justo mattis libero pharetra aliquet', 'genius-addon-lite' )
			]
		);
		$this->add_control(
			'media',
			[
				'label'       => esc_html__( 'Image', 'genius-addon-lite' ),
				'type'        => Controls_Manager::MEDIA,
				'description' => esc_html__( 'select Image.', 'genius-addon-lite' ),
			]
		);
		$this->add_control(
			'read_more_text',
			[
				'label'       => esc_html__( 'Read More Text', 'genius-addon-lite' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Read more', 'genius-addon-lite' ),
				'description' => esc_html__( 'enter readmore text', 'genius-addon-lite' ),
			]
		);
		$this->add_control(
			'link',
			[
				'label'       => esc_html__( 'Link', 'genius-addon-lite' ),
				'type'        => Controls_Manager::URL,
				'description' => esc_html__( 'enter link', 'genius-addon-lite' ),
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'styling_section',
			[
				'label' => esc_html__( 'Styling Settings', 'genius-addon-lite' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control( Group_Control_Background::get_type(), [
			'name'     => 'overlay_color',
			'label'    => esc_html__( 'Overlay Color', 'genius-addon-lite' ),
			'selector' => "{{WRAPPER}} .image-single-item:hover .content::after, {{WRAPPER}} .image-single-item.active .content::after"
		] );

		$this->add_control( 'title_color', [
			'label'     => esc_html__( 'Title Color', 'genius-addon-lite' ),
			'type'      => Controls_Manager::COLOR,
			'selectors' => [
				"{{WRAPPER}} .image-single-item .content .title" => "color: {{VALUE}}"
			]
		] );
		$this->add_control( 'description_color', [
			'label'     => esc_html__( 'Description Color', 'genius-addon-lite' ),
			'type'      => Controls_Manager::COLOR,
			'selectors' => [
				"{{WRAPPER}} .image-single-item .content p" => "color: {{VALUE}}"
			]
		] );

		$this->add_control( 'readmore_color', [
			'label'     => esc_html__( 'Read More Color', 'genius-addon-lite' ),
			'type'      => Controls_Manager::COLOR,
			'selectors' => [
				"{{WRAPPER}} .image-single-item .content .btn-wrapper .read-btn" => "color: {{VALUE}}"
			]
		] );

		$this->end_controls_section();


		$this->start_controls_section(
			'typography_settings_section',
			[
				'label' => esc_html__( 'Typography Settings', 'genius-addon-lite' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control( Group_Control_Typography::get_type(), [
			'name'     => 'title_typography',
			'label'    => esc_html__( 'Title Typography', 'genius-addon-lite' ),
			'selector' => "{{WRAPPER}} .image-single-item .content .title"
		] );

		$this->add_group_control( Group_Control_Typography::get_type(), [
			'name'     => 'description_typography',
			'label'    => esc_html__( 'Description Typography', 'genius-addon-lite' ),
			'selector' => "{{WRAPPER}} .image-single-item .content p"
		] );
		$this->add_group_control( Group_Control_Typography::get_type(), [
			'name'     => 'readmore_typography',
			'label'    => esc_html__( 'Read More Typography', 'genius-addon-lite' ),
			'selector' => "{{WRAPPER}} .image-single-item .content .btn-wrapper .read-btn"
		] );
		$this->end_controls_section();
	}

	/**
	 * Render Elementor widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
		$this->add_render_attribute( 'image_box_wrapper', 'class', 'image-single-item' );

		if ($settings['active_widget'] === 'yes'){
			$this->add_render_attribute( 'image_box_wrapper', 'class', 'active' );
		}


		$this->add_render_attribute( 'image_box_link', 'class', '' );
		$this->add_render_attribute( 'image_box_btn', 'class', 'read-btn' );
		if ( ! empty( $settings['link']['url'] ) ) {
			$this->add_link_attributes( 'image_box_link', $settings['link'] );
			$this->add_link_attributes( 'image_box_btn', $settings['link'] );
		}
		$image_id  = $settings['media']['id'];
		$image_url = ! empty( $image_id ) ? wp_get_attachment_image_src( $image_id, 'full' )[0] : '';
		$image_alt = ! empty( $image_id ) ? get_post_meta( $image_id, '_wp_attachment_image_alt', true ) : '';
		?>
		<div <?php echo esc_url($this->get_render_attribute_string( 'image_box_wrapper' )); ?>>
			<div class="thumb">
				<img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>">
			</div>
			<div class="content" style="background-image:url(<?php echo esc_url($image_url);?>);">
				<div class="content-wrapper">
					<a <?php echo $this->get_render_attribute_string( 'image_box_link' ); ?>>
						<h3 class="title"><?php echo esc_html( $settings['title'] ) ?></h3></a>
					<p><?php echo esc_html( $settings['description'] ) ?></p>
					<div class="btn-wrapper">
						<a <?php echo esc_url($this->get_render_attribute_string( 'image_box_btn' )); ?>><?php echo esc_html( $settings['read_more_text'] ); ?>
							<i class="fas fa-long-arrow-alt-right"></i></a>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new Genius_Image_Hover() );