<?php

namespace Elementor;
/**
 * Package Genius Addon
 * Author Xgenious
 * @since 1.0.0
 * */

if (!defined('ABSPATH')) {
    exit(); //exit if access directly
}

class Genius_Alert_Block_One extends Widget_Base
{

    /**
     * Get widget name.
     *
     * Retrieve Elementor widget name.
     *
     * @return string Widget name.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_name()
    {
        return 'genius-alert-block-one-widget';
    }

    /**
     * Get widget title.
     *
     * Retrieve Elementor widget title.
     *
     * @return string Widget title.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_title()
    {
        return esc_html__('Alert Block', 'genius-addon-lite');
    }

    /**
     * Get widget icon.
     *
     * Retrieve Elementor widget icon.
     *
     * @return string Widget icon.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_icon()
    {
        return 'eicon-alert xga';
    }

    /**
     * Get widget categories.
     *
     * Retrieve the list of categories the Elementor widget belongs to.
     *
     * @return array Widget categories.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_categories()
    {
        return ['ega-widgets'];
    }

    /**
     * Register Elementor widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function _register_controls()
    {

        $this->start_controls_section(
            'settings_section',
            [
                'label' => esc_html__('Alert Block', 'genius-addon-lite'),
                'tab' => Controls_Manager::TAB_CONTENT,
            ]
        );
        $this->add_control('text', [
            'label' => esc_html__('Text', 'genius-addon-lite'),
            'type' => Controls_Manager::TEXT,
            'default' => esc_html__('Alert Block', 'genius-addon-lite')
        ]);
        $this->add_control('alert_type', [
            'label' => esc_html__('Alert Type', 'genius-addon-lite'),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'success' => esc_html__('Success', 'genius-addon-lite'),
                'primary' => esc_html__('Primary', 'genius-addon-lite'),
                'secondary' => esc_html__('Secondary', 'genius-addon-lite'),
                'info ' => esc_html__('Info', 'genius-addon-lite'),
                'warning' => esc_html__('Warning', 'genius-addon-lite'),
                'danger' => esc_html__('Danger', 'genius-addon-lite'),
                'light' => esc_html__('Light', 'genius-addon-lite'),
                'dark' => esc_html__('Dark', 'genius-addon-lite'),
            ],
            'default' => 'success'
        ]);
        $this->add_control('is_dismissible', [
            'label' => esc_html__('Is Dismissible', 'genius-addon-lite'),
            'type' => Controls_Manager::SWITCHER,
            'default' => 'no',
        ]);
        if (genius_version_compare(ELEMENTOR_VERSION, '2.6.0', '>=')) {
            $this->add_control('icon', [
                'label' => esc_html__('Icon', 'genius-addon-lite'),
                'type' => Controls_Manager::ICONS,
            ]);
            $icon_condition = ['icon[value]!' => ''];
        } else {
            $this->add_control('icon', [
                'label' => esc_html__('Icon', 'genius-addon-lite'),
                'type' => Controls_Manager::ICON,
            ]);
            $icon_condition = ['icon!' => ''];
        }

        $this->add_control('icon_position', [
            'label' => esc_html__('Icon Position', 'genius-addon-lite'),
            'type' => Controls_Manager::CHOOSE,
            'options' => [
                'left' => [
                    'icon' => 'fa fa-arrow-left',
                    'title' => esc_html__('Left', 'genius-addon-lite'),
                ],
                'top' => [
                    'icon' => 'fa fa-arrow-up',
                    'title' => esc_html__('Top', 'genius-addon-lite'),
                ],
                'bottom' => [
                    'icon' => 'fa fa-arrow-down',
                    'title' => esc_html__('Bottom', 'genius-addon-lite'),
                ],
                'right' => [
                    'icon' => 'fa fa-arrow-right',
                    'title' => esc_html__('Right', 'genius-addon-lite'),
                ],
            ],
            'default' => 'left',
            'condition' => $icon_condition

        ]);
        $this->add_responsive_control('alignment', [
            'label' => esc_html__('Alignment', 'genius-addon-lite'),
            'type' => Controls_Manager::CHOOSE,
            'options' => [
                'left' => [
                    'icon' => 'fa fa-align-left',
                    'title' => esc_html__('Left', 'genius-addon-lite')
                ],
                'center' => [
                    'icon' => 'fa fa-align-center',
                    'title' => esc_html__('Center', 'genius-addon-lite')
                ],
                'right' => [
                    'icon' => 'fa fa-align-right',
                    'title' => esc_html__('Right', 'genius-addon-lite')
                ]
            ],
            'default' => 'left',
            'selectors' => [
                "{{WRAPPER}} .ega-alert-block-wrapper" => "text-align: {{VALUE}}"
            ]
        ]);
        $this->end_controls_section();
        $this->start_controls_section('alert-block-style', [
            'label' => esc_html__('Alert Block Style', 'genius-addon-lite'),
            'tab' => Controls_Manager::TAB_STYLE
        ]);
        $this->start_controls_tabs('alert-block-tab-wrap');
        $this->start_controls_tab('alert-block-normal', [
            'label' => esc_html__('Normal', 'genius-addon-lite')
        ]);
        $this->add_control('alert-color', [
            'label' => esc_html__('Color', 'genius-addon-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .ega-alert" => "color: {{VALUE}}"
            ]
        ]);
        $this->add_group_control(Group_Control_Typography::get_type(), [
            'label' => esc_html__('Typography', 'genius-addon-lite'),
            'name' => 'alert-block-typography',
            'selector' => "{{WRAPPER}} .ega-alert"
        ]);
        $this->add_group_control(Group_Control_Background::get_type(), [
            'label' => esc_html__('Background', 'genius-addon-lite'),
            'name' => 'alert-block-background',
            'selector' => "{{WRAPPER}} .ega-alert"
        ]);
        $this->add_control('alert-block-padding', [
            'label' => esc_html__('Padding', 'genius-addon-lite'),
            'type' => Controls_Manager::DIMENSIONS,
            'selectors' => [
                "{{WRAPPER}} .ega-alert" => "padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};"
            ]
        ]);
        $this->add_group_control(Group_Control_Border::get_type(), [
            'label' => esc_html__('Border', 'genius-addon-lite'),
            'name' => 'alert-block-border',
            'selector' => "{{WRAPPER}} .ega-alert"
        ]);
        $this->add_group_control(Group_Control_Box_Shadow::get_type(), [
            'label' => esc_html__('Box Shadow', 'genius-addon-lite'),
            'name' => 'alert-block-box-shadow',
            'selector' => "{{WRAPPER}} .ega-alert"
        ]);
        $this->add_responsive_control('alert-block-border-radius', [
            'label' => esc_html__('Border Radius', 'genius-addon-lite'),
            'type' => Controls_Manager::DIMENSIONS,
            'size_units' => ['px'],
            'selectors' => [
                "{{WRAPPER}} .ega-alert" => "border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}"
            ]
        ]);
        $this->end_controls_tab();
        $this->start_controls_tab('alert-block-hover', [
            'label' => esc_html__('Hover', 'genius-addon-lite')
        ]);
        $this->add_control('alert-hover-color', [
            'label' => esc_html__('Color', 'genius-addon-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .ega-alert:hover" => "color: {{VALUE}}"
            ]
        ]);
        $this->add_group_control(Group_Control_Background::get_type(), [
            'label' => esc_html__('Background', 'genius-addon-lite'),
            'name' => 'alert-block-hover-background',
            'selector' => "{{WRAPPER}} .ega-alert:hover"
        ]);
        $this->add_group_control(Group_Control_Border::get_type(), [
            'label' => esc_html__('Border', 'genius-addon-lite'),
            'name' => 'alert-block-hover-border',
            'selector' => "{{WRAPPER}} .ega-alert:hover"
        ]);
        $this->add_group_control(Group_Control_Box_Shadow::get_type(), [
            'label' => esc_html__('Box Shadow', 'genius-addon-lite'),
            'name' => 'alert-block-hover-box-shadow',
            'selector' => "{{WRAPPER}} .ega-alert:hover"
        ]);
        $this->end_controls_tab();
        $this->end_controls_tabs();
        $this->end_controls_section();
        $this->start_controls_section('alert-block-icon-style', [
            'label' => esc_html__('Icon Style', 'genius-addon-lite'),
            'tab' => Controls_Manager::TAB_STYLE
        ]);
        $this->start_controls_tabs('alert-block-icon-style-wrap');
        $this->start_controls_tab('alert-block-icon-style-normal', [
            'label' => esc_html__('Normal', 'genius-addon-lite')
        ]);
        $this->add_control('alert-icon-color', [
            'label' => esc_html__('Color', 'genius-addon-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .ega-alert .ega-alert-icon" => "color: {{VALUE}}"
            ]
        ]);
        $this->add_group_control(Group_Control_Background::get_type(), [
            'label' => esc_html__('Background', 'genius-addon-lite'),
            'name' => 'alert-block-icon-background',
            'selector' => "{{WRAPPER}} .ega-alert .ega-alert-icon"
        ]);
        $this->add_control('alert-block-icon-padding', [
            'label' => esc_html__('Padding', 'genius-addon-lite'),
            'type' => Controls_Manager::DIMENSIONS,
            'selectors' => [
                "{{WRAPPER}} .ega-alert .ega-alert-icon" => "padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};"
            ]
        ]);
        $this->add_group_control(Group_Control_Border::get_type(), [
            'label' => esc_html__('Border', 'genius-addon-lite'),
            'name' => 'alert-block-icon-border',
            'selector' => "{{WRAPPER}} .ega-alert .ega-alert-icon"
        ]);
        $this->add_group_control(Group_Control_Box_Shadow::get_type(), [
            'label' => esc_html__('Box Shadow', 'genius-addon-lite'),
            'name' => 'alert-block-icon-box-shadow',
            'selector' => "{{WRAPPER}} .ega-alert .ega-alert-icon"
        ]);
        $this->add_responsive_control('alert-block-icon-border-radius', [
            'label' => esc_html__('Border Radius', 'genius-addon-lite'),
            'type' => Controls_Manager::DIMENSIONS,
            'size_units' => ['px'],
            'selectors' => [
                "{{WRAPPER}} .ega-alert .ega-alert-icon" => "border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}"
            ]
        ]);
        $this->end_controls_tab();
        $this->start_controls_tab('alert-block-icon-hover-style', [
            'label' => esc_html__('Hover', 'genius-addon-lite')
        ]);
        $this->add_control('alert-icon-hover-color', [
            'label' => esc_html__('Color', 'genius-addon-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .ega-alert:hover .ega-alert-icon" => "color: {{VALUE}}"
            ]
        ]);
        $this->add_group_control(Group_Control_Background::get_type(), [
            'label' => esc_html__('Background', 'genius-addon-lite'),
            'name' => 'alert-block-icon-hover-background',
            'selector' => "{{WRAPPER}} .ega-alert:hover .ega-alert-icon"
        ]);
        $this->add_group_control(Group_Control_Box_Shadow::get_type(), [
            'label' => esc_html__('Box Shadow', 'genius-addon-lite'),
            'name' => 'alert-block-icon-hover-box-shadow',
            'selector' => "{{WRAPPER}} .ega-alert:hover .ega-alert-icon"
        ]);
        $this->add_group_control(Group_Control_Border::get_type(), [
            'label' => esc_html__('Border', 'genius-addon-lite'),
            'name' => 'alert-block-hover-icon-border',
            'selector' => "{{WRAPPER}} .ega-alert:hover .ega-alert-icon"
        ]);
        $this->end_controls_tab();
        $this->end_controls_tabs();
        $this->end_controls_section();
    }

    /**
     * Render Elementor widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $this->add_render_attribute('ega-alert-block-wrapper', 'class', 'ega-alert-block-wrapper');
        $this->add_render_attribute('ega-alert-block-wrapper', 'class', 'ega-alert-block-icon-position-' . $settings['icon_position']);
        $this->add_render_attribute('ega-alert', 'class', 'ega-alert');
        $this->add_render_attribute('ega-alert', 'class', 'ega-alert-' . $settings['alert_type']);
        $close_btn = '';
        if ('yes' == $settings['is_dismissible']) {
            $this->add_render_attribute('ega-alert-block-wrapper', 'class', 'ega-alert-dismissible');
            $close_btn = '<span aria-hidden="true" onclick="this.parentNode.parentNode.remove()" class="ega-close">&times;</span>';
        }
        if (genius_version_compare(ELEMENTOR_VERSION, '2.6.0', '>=')) {
            $icon = !empty($settings['icon']['value']) ? '<span class="ega-alert-icon">' . Genius_Icon_Manager::render_icon($settings['icon'], ['aria-hidden' => 'true']) . '</span>' : '';
        } else {
            $icon = !empty($settings['icon']) ? sprintf('<span class="ega-alert-icon">%1$s</span>',esc_attr( $settings['icon'])) : '';
        }

        printf('<div %1$s><div %2$s>%3$s %4$s %5$s</div></div>',
            $this->get_render_attribute_string('ega-alert-block-wrapper'),
            $this->get_render_attribute_string('ega-alert'),
            $icon,
            esc_html($settings['text']),
            $close_btn
        );
    }
}

Plugin::instance()->widgets_manager->register_widget_type(new Genius_Alert_Block_One());