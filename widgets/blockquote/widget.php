<?php

namespace Elementor;
/**
 * Package genius Addon
 * Author Xgenious
 * @since 1.0.0
 * */

if (!defined('ABSPATH')) {
    exit(); //exit if access directly
}

class Genius_Blockquote_One extends Widget_Base
{

    /**
     * Get widget name.
     *
     * Retrieve Elementor widget name.
     *
     * @return string Widget name.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_name()
    {
        return 'genius-blockquote-one-widget';
    }

    /**
     * Get widget title.
     *
     * Retrieve Elementor widget title.
     *
     * @return string Widget title.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_title()
    {
        return esc_html__('Blockquote', 'genius-addon-lite');
    }

    /**
     * Get widget icon.
     *
     * Retrieve Elementor widget icon.
     *
     * @return string Widget icon.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_icon()
    {
        return 'eicon-blockquote xga';
    }

    /**
     * Get widget categories.
     *
     * Retrieve the list of categories the Elementor widget belongs to.
     *
     * @return array Widget categories.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_categories()
    {
        return ['ega-widgets'];
    }

    /**
     * Register Elementor widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function _register_controls()
    {

        $this->start_controls_section(
            'settings_section',
            [
                'label' => esc_html__('Blockquote', 'genius-addon-lite'),
                'tab' => Controls_Manager::TAB_CONTENT,
            ]
        );
        $this->add_control(
            'title', [
                'label'       => esc_html__( 'Author Name', 'genius-addon-lite' ),
                'type'        => Controls_Manager::TEXT,
                'description' => esc_html__( 'enter title.', 'genius-addon-lite' ),
                'default'     => esc_html__( 'Sharifur Robin', 'genius-addon-lite' )
            ]
        );
        $this->add_control(
            'description', [
                'label'       => esc_html__( 'Author Quote', 'genius-addon-lite' ),
                'type'        => Controls_Manager::TEXTAREA,
                'description' => esc_html__( 'enter text.', 'genius-addon-lite' ),
                'default'     => esc_html__( 'Silent sir say desire fat him letter. Whatever settling goodness too and
                honoured she building answered her. Strongly thoughts remember mr to do consider debating. Spirits
                musical behaved on we he farther letters. Repulsive he he as deficient newspaper', 'genius-addon-lite' )
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'styling_section',
            [
                'label' => esc_html__( 'Styling Settings', 'genius-addon-lite' ),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control( 'title_color', [
            'label'     => esc_html__( 'Author Name Color', 'genius-addon-lite' ),
            'type'      => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .ega-wp-block-quote p" => "color: {{VALUE}}"
            ]
        ] );
        $this->add_control( 'icon_color', [
            'label'     => esc_html__( 'Quote Icon Color', 'genius-addon-lite' ),
            'type'      => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .ega-wp-block-quote:before" => "color: {{VALUE}}"
            ]
        ] );
        $this->add_control( 'description_color', [
            'label'     => esc_html__( 'Author Quote Color', 'genius-addon-lite' ),
            'type'      => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .ega-wp-block-quote cite" => "color: {{VALUE}}"
            ]
        ] );

        $this->end_controls_section();

        $this->start_controls_section(
            'typography_settings_section',
            [
                'label' => esc_html__('Typography Settings', 'genius-addon-lite'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_group_control(Group_Control_Typography::get_type(), [
            'label' => esc_html__('Author Title Typography', 'genius-addon-lite'),
            'name' => 'title_typography',
            'description' => esc_html__('select title typography', 'genius-addon-lite'),
            'selector' => "{{WRAPPER}} .ega-wp-block-quote cite"
        ]);
        $this->add_group_control(Group_Control_Typography::get_type(), [
            'label' => esc_html__('Author Quote Typography', 'genius-addon-lite'),
            'name' => 'paragraph_typography',
            'description' => esc_html__('select Paragraph typography', 'genius-addon-lite'),
            'selector' => "{{WRAPPER}} .ega-wp-block-quote p"
        ]);
        $this->end_controls_section();
    }

    /**
     * Render Elementor widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function render()
    {
        $settings = $this->get_settings_for_display();
        ?>
        <blockquote class="ega-wp-block-quote">
            <p>
                <?php echo esc_html($settings['description']);?>
            </p>
            <cite><?php echo esc_html($settings['title']);?></cite>
        </blockquote>
        <?php
    }
}

Plugin::instance()->widgets_manager->register_widget_type(new Genius_Blockquote_One());