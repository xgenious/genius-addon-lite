<?php

namespace Elementor;
/**
 * Package genius Addon
 * Author Xgenious
 * @since 1.0.0
 * */

if (!defined('ABSPATH')) {
    exit(); //exit if access directly
}


class Genius_Tabs_One extends Widget_Base
{

    /**
     * Get widget name.
     *
     * Retrieve Elementor widget name.
     *
     * @return string Widget name.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_name()
    {
        return 'genius-tabs-one-widget';
    }

    /**
     * Get widget title.
     *
     * Retrieve Elementor widget title.
     *
     * @return string Widget title.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_title()
    {
        return esc_html__('Tabs', 'genius-addon-lite');
    }

    /**
     * Get widget icon.
     *
     * Retrieve Elementor widget icon.
     *
     * @return string Widget icon.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_icon()
    {
        return 'eicon-product-tabs xga';
    }

    /**
     * Get widget categories.
     *
     * Retrieve the list of categories the Elementor widget belongs to.
     *
     * @return array Widget categories.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_categories()
    {
        return ['ega-widgets'];
    }

    /**
     * Register Elementor widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function _register_controls()
    {

        $this->start_controls_section(
            'settings_section',
            [
                'label' => esc_html__('General Settings', 'genius-addon-lite'),
                'tab' => Controls_Manager::TAB_CONTENT,
            ]
        );
        $repeater = new Repeater();

        $repeater->add_control(
            'nav_title',
            [
                'label' => esc_html__('Nav Title', 'genius-addon-lite'),
                'type' => Controls_Manager::TEXT,
                'description' => esc_html__('enter title.', 'genius-addon-lite'),
                'default' => esc_html__('Free', 'genius-addon-lite'),
            ]
        );
        $repeater->add_control(
            'title_status', [
                'label' => esc_html__('Title Show/Hide', 'genius-addon-lite'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'description' => esc_html__('show/hide title', 'genius-addon-lite')
            ]
        );
        $repeater->add_control(
            'title',
            [
                'label' => esc_html__('Title', 'genius-addon-lite'),
                'type' => Controls_Manager::TEXT,
                'description' => esc_html__('enter title.', 'genius-addon-lite'),
                'default' => esc_html__('Drag & Drop Widgets', 'genius-addon-lite'),
            ]
        );
        $repeater->add_control(
            'link',
            [
                'label' => esc_html__('Link', 'genius-addon-lite'),
                'type' => Controls_Manager::URL,
                'description' => esc_html__('enter url.', 'genius-addon-lite'),
                'default' => [
                    'url' => ''
                ]
            ]
        );
        $repeater->add_control(
            'icon',
            [
                'label' => esc_html__('Icon', 'genius-addon-lite'),
                'type' => Controls_Manager::ICONS,
                'description' => esc_html__('select Icon.', 'genius-addon-lite'),
                'default' => [
                    'value' => 'fas fa-icons',
                    'library' => 'solid',
                ]
            ]
        );
        $repeater->add_control(
            'description_status', [
                'label' => esc_html__('Description Show/Hide', 'genius-addon-lite'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'description' => esc_html__('show/hide description', 'genius-addon-lite')
            ]
        );
        $repeater->add_control(
            'description',
            [
                'label' => esc_html__('Description', 'genius-addon-lite'),
                'type' => Controls_Manager::TEXTAREA,
                'description' => esc_html__('enter text.', 'genius-addon-lite'),
                'default' => esc_html__('Drag And Drop Widgets Are One Of The Newest Ways To Make Your WordPress Site Easier To Built With.', 'genius-addon-lite')
            ]
        );
        $repeater->add_control(
            'text_align',
            [
                'label' => esc_html__('Alignment', 'genius-addon-lite'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => esc_html__('Left', 'genius-addon-lite'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => esc_html__('Center', 'genius-addon-lite'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => esc_html__('Right', 'genius-addon-lite'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'default' => 'center',
                'toggle' => true,
            ]
        );
        $this->add_control(

            'icon_box_item', [
                'label' => esc_html__('Icon Box Item', 'libo-master'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
            ]

        );
        $this->end_controls_section();


        $this->start_controls_section(
            'icon_styling_settings_section',
            [
                'label' => esc_html__('Icon Styling Settings', 'genius-addon-lite'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_control(
            'icon_height',
            [
                'label' => esc_html__('Icon Height', 'aapside-master'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%', 'em'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 10,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .icon-box-item .icon' => 'height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->add_control(
            'icon_width',
            [
                'label' => esc_html__('Icon Width', 'aapside-master'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%', 'em'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 10,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .icon-box-item .icon' => 'width: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->add_control(
            'icon_size',
            [
                'label' => esc_html__('Icon Size', 'genius-addon-lite'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%', 'em'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 10,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                    'size' => 35,
                ],
                'selectors' => [
                    '{{WRAPPER}} .icon-box-item .icon' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->add_control(
            'icon_margin_bottom',
            [
                'label' => esc_html__('Icon Margin Bottom', 'genius-addon-lite'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%', 'em'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 10,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .icon-box-item .icon' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->add_control(
            'position',
            [
                'label' => esc_html__('Position', 'genius-addon-lite'),
                'type' => Controls_Manager::SELECT,
                'default' => 'top',
                'options' => [
                    'top' => esc_html__('Top', 'genius-addon-lite'),
                    'left' => esc_html__('Left', 'genius-addon-lite'),
                    'right' => esc_html__('Right', 'genius-addon-lite'),
                ],
            ]
        );
        $this->add_control(
            'title_margin_bottom',
            [
                'label' => esc_html__('Title Margin Bottom', 'genius-addon-lite'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%', 'em'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 10,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .icon-box-item .content .title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->start_controls_tabs(
            'icon_box_style_tabs'
        );

        $this->start_controls_tab(
            'icon_box_normal_style_normal_tab',
            [
                'label' => esc_html__('Normal', 'genius-addon-lite'),
            ]
        );
        $this->add_control('icon_color', [
            'label' => esc_html__('Icon Color', 'genius-addon-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .icon-box-item .icon" => "color: {{VALUE}}"
            ]
        ]);
        $this->add_group_control(Group_Control_Background::get_type(), [
            'name' => 'icon_bg_color',
            'label' => esc_html__('Icon Background', 'genius-addon-lite'),
            'selector' => "{{WRAPPER}} .icon-box-item .icon"

        ]);
        $this->end_controls_tab();

        $this->start_controls_tab(
            'icon_box_hover_style_hover_tab',
            [
                'label' => esc_html__('Hover', 'genius-addon-lite'),
            ]
        );
        $this->add_control('icon_hover_color', [
            'label' => esc_html__('Icon Color', 'genius-addon-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .icon-box-item:hover .icon" => "color: {{VALUE}}"
            ]
        ]);
        $this->add_group_control(Group_Control_Background::get_type(), [
            'name' => 'icon_bg_hover_color',
            'label' => esc_html__('Icon Background Color', 'genius-addon-lite'),
            'selector' => "{{WRAPPER}} .icon-box-item:hover .icon"

        ]);
        $this->end_controls_tab();

        $this->end_controls_tabs();
        $this->end_controls_section();


        $this->start_controls_section(
            'box_styling_settings_section',
            [
                'label' => esc_html__('Box Styling Settings', 'genius-addon-lite'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->start_controls_tabs(
            'slider_nav_style_tabs'
        );

        $this->start_controls_tab(
            'active_hover_style_normal_tab',
            [
                'label' => esc_html__('Normal', 'genius-addon-lite'),
            ]
        );
        $this->add_control(
            'item_padding',
            [
                'label' => esc_html__('Padding', 'genius-addon-lite'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .icon-box-item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'border',
                'label' => esc_html__('Border', 'genius-addon-lite'),
                'selector' => '{{WRAPPER}} .icon-box-item',
            ]
        );

        $this->add_control(
            'background_border_radius',
            [
                'label' => esc_html__('Border Radius', 'genius-addon-lite'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .icon-box-item' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'box_shadow',
                'label' => esc_html__('Box Shadow', 'genius-addon-lite'),
                'selector' => '{{WRAPPER}} .icon-box-item',
            ]
        );
        $this->add_control('background_color', [
            'label' => esc_html__('BOx Background Color', 'genius-addon-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .icon-box-item" => "background-color: {{VALUE}}"
            ]
        ]);
        $this->add_control('title_color', [
            'label' => esc_html__('Title Color', 'genius-addon-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .icon-box-item .content .title" => "color: {{VALUE}}"
            ]
        ]);
        $this->add_control('paragraph_color', [
            'label' => esc_html__('Paragraph Color', 'genius-addon-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .icon-box-item .content p" => "color: {{VALUE}}"
            ]
        ]);
        $this->end_controls_tab();

        $this->start_controls_tab(
            'slider_navigation_style_hover_tab',
            [
                'label' => esc_html__('Hover', 'genius-addon-lite'),
            ]
        );
        $this->add_control('background_hover_color', [
            'label' => esc_html__('Background Color', 'genius-addon-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .icon-box-item:hover" => "background-color: {{VALUE}}"
            ]
        ]);
        $this->add_control('title_hover_color', [
            'label' => esc_html__('Title Color', 'genius-addon-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .icon-box-item:hover .content .title" => "color: {{VALUE}}"
            ]
        ]);
        $this->add_control('paragraph_hover_color', [
            'label' => esc_html__('Paragraph Color', 'genius-addon-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                "{{WRAPPER}} .icon-box-item:hover .content p" => "color: {{VALUE}}"
            ]
        ]);
        $this->end_controls_tab();

        $this->end_controls_tabs();
        $this->end_controls_section();

        $this->start_controls_section(
            'typography_settings_section',
            [
                'label' => esc_html__('Typography Settings', 'genius-addon-lite'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'label' => esc_html__('Title Typography', 'genius-addon-lite'),
                'selector' => '{{WRAPPER}} .icon-box-item .content .title',
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'description_typography',
                'label' => esc_html__('Paragraph Typography', 'genius-addon-lite'),
                'selector' => '{{WRAPPER}} .icon-box-item .content p',
            ]
        );
        $this->end_controls_section();

    }

    /**
     * Render Elementor widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $random_number = rand(3333, 9999999);
        $accordion_items = $settings['icon_box_item'];
        if (!empty($settings['link']['url'])) {
            $this->add_link_attributes('link_wrapper', $settings['link']);
        }
        ?>
        <div class="xga-tabs-wrapper xg_tab_styling_class_<?php echo esc_attr($random_number) ?>"
             id="xga-tabs">
            <div class="xgenious-tab-wrapper">
                <ul class="xgnav-tabs">
                    <?php

                    $a = 0;
                    $randorm_number = [];
                    foreach ($accordion_items as $item_nav) :
                        $active_class = (0 == $a) ? 'active' : '';
                        $a++;
                        $rand_val = rand(111, 999999999);
                        $randorm_number[] = $rand_val;
                        ?>
                        <li class="<?php echo esc_attr($active_class); ?>"
                            data-target="#xg_tabs_id_<?php echo esc_attr($rand_val); ?>"> <?php echo esc_html( $item_nav['nav_title'] ); ?>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <div class="xgtab-content">
                    <?php
                    $c = 0;
                    foreach ($accordion_items as $key =>  $item) :
                        $tab_content_active_class = (0 == $c) ? 'active' : 'fade';
                        $c++;
                        ?>
                        <div class="xgtab-pane <?php echo esc_attr($tab_content_active_class) ?>"
                             id="xg_tabs_id_<?php echo esc_attr($randorm_number[$key]); ?>">
                            <div class="icon-box-item <?php echo esc_html($settings['position']) ?>"
                                 style="text-align:<?php echo esc_attr($item['text_align']); ?>">
                                <div class="icon-wrap">
                                    <div class="icon">
                                        <?php
                                        Icons_Manager::render_icon($item['icon'], ['aria-hidden' => 'true']);
                                        ?>
                                    </div>
                                </div>
                                <div class="content">
                                    <?php
                                    if (!empty($item['title_status'])) {
                                        printf('<a %1$s ><h3 class="title">%2$s</h3></a>', $this->get_render_attribute_string('link_wrapper'), esc_html($item['title']));
                                    }
                                    if (!empty($item['description_status'])) {
                                        printf('<p>%1$s</p>', esc_html($item['description']));
                                    } ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <?php
    }
}

Plugin::instance()->widgets_manager->register_widget_type(new Genius_Tabs_One());