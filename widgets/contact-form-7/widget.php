<?php

namespace Elementor;
/**
 * Package genius Addon
 * Author Xgenious
 * @since 1.0.0
 * */

if (!defined('ABSPATH')) {
    exit(); //exit if access directly
}

class Genius_Contact_Form extends Widget_Base
{

    /**
     * Get widget name.
     *
     * Retrieve Elementor widget name.
     *
     * @return string Widget name.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_name()
    {
        return 'genius-contact-form-widget';
    }

    /**
     * Get widget title.
     *
     * Retrieve Elementor widget title.
     *
     * @return string Widget title.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_title()
    {
        return esc_html__('Contact Form 7', 'genius-addon-lite');
    }

    /**
     * Get widget icon.
     *
     * Retrieve Elementor widget icon.
     *
     * @return string Widget icon.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_icon()
    {
        return 'eicon-form-horizontal xga';
    }

    /**
     * Get widget categories.
     *
     * Retrieve the list of categories the Elementor widget belongs to.
     *
     * @return array Widget categories.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_categories()
    {
        return ['ega-widgets'];
    }

    /**
     * Register Elementor widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     * @access protected
     */

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_my_custom',
            [
                'label' => esc_html__('Team Filter', 'genius-addon-lite'),
            ]
        );
        $this->add_control(
            'xga_contact_form_id',
            [
                'label'   => esc_html__( 'Contact Form', 'genius-addon-lite' ),
                'type'    => Controls_Manager::SELECT,
                'options' => xga_get_contact_form_shortcode_list_el(),
            ]
        );
        /**
         * End Title Section
         */
        $this->end_controls_section();

    }

    /**
     * Define our Content Display Settings
     */
    protected function render()
    {
        $settings = $this->get_settings();
        /**
         * main part
         */
        $shortcode = $settings['xga_contact_form_id'];
        if (!empty($shortcode)):
            ?>
            <div class="elementor-shortcode">
                <?php
                echo wp_kses($do_shortcode('[contact-form-7  id="' . $shortcode . '"]'),genius_allowed_html_tags('all'));
                ?>
            </div>
        <?php
        else:
             esc_html_e('please select and shortcode first','genius-addon-lite');
        endif;
    }
}

Plugin::instance()->widgets_manager->register_widget_type(new Genius_Contact_Form());