<?php
namespace Elementor;
/**
 * Package genius Addon
 * Author Xgenious
 * @since 1.0.0
 * */

if (!defined('ABSPATH')){
	exit(); //exit if access directly
}



class Genius_Accordion_One extends Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Elementor widget name.
	 *
	 * @return string Widget name.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_name() {
		return 'genius-accordion-one-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Elementor widget title.
	 *
	 * @return string Widget title.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_title() {
		return esc_html__( 'Accordion', 'genius-addon-lite' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Elementor widget icon.
	 *
	 * @return string Widget icon.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_icon() {
		return 'eicon-accordion xga';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Elementor widget belongs to.
	 *
	 * @return array Widget categories.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_categories() {
		return [ 'ega-widgets' ];
	}

	/**
	 * Register Elementor widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

        $this->start_controls_section(
            'settings_section',
            [
                'label' => esc_html__( 'General Settings', 'genius-addon-lite' ),
                'tab'   => Controls_Manager::TAB_CONTENT,
            ]
        );
        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'open',
            [
                'label' => esc_html__('Column', 'genius-addon-lite'),
                'type' => Controls_Manager::SELECT,
                'options' => array(
                    'open' => esc_html__('Open', 'genius-addon-lite'),
                    '' => esc_html__('Close', 'genius-addon-lite'),
                ),
                'description' => esc_html__('select grid column', 'genius-addon-lite'),
            ]
        );
        $repeater->add_control(
            'title', [
                'label'       => esc_html__( 'Title', 'genius-addon-lite' ),
                'type'        => Controls_Manager::TEXT,
                'description' => esc_html__( 'enter title.', 'genius-addon-lite' ),
                'default'     => esc_html__( 'How to import my whole data?', 'genius-addon-lite' )
            ]
        );
        $repeater->add_control(
            'description', [
                'label'       => esc_html__( 'Description', 'genius-addon-lite' ),
                'type'        => Controls_Manager::TEXTAREA,
                'description' => esc_html__( 'enter text.', 'genius-addon-lite' ),
                'default'     => esc_html__( 'Duis aute irure dolor reprehenderit in voluptate velit essle cillum dolore eu fugiat nulla pariatur. Excepteur sint ocaec at cupdatat proident suntin culpa qui officia deserunt mol anim id esa laborum perspiciat.', 'genius-addon-lite' )
            ]
        );
        $this->add_control('accordion_items', [
            'label' => esc_html__('Accordion Item', 'genius-addon-lite'),
            'type' => Controls_Manager::REPEATER,
            'fields' => $repeater->get_controls(),
            'default' => [
                [
                    'title'        => esc_html__( 'How to import my whole data?', 'genius-addon-lite' ),
                    'description' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco lab oris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum.','genius-addon-lite'),
                ]
            ],

        ]);
        $this->end_controls_section();


        /*  tab styling tabs start */
        $this->start_controls_section(
            'tab_settings_section',
            [
                'label' => esc_html__('Tab Settings', 'genius-addon-lite'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->start_controls_tabs(
            'tab_style_tabs'
        );

        $this->start_controls_tab(
            'tab_style_normal_tab',
            [
                'label' => __('Expanded Style', 'genius-addon-lite'),
            ]
        );

        $this->add_control('tab_color', [
            'type' => Controls_Manager::COLOR,
            'label' => esc_html__('Title Color', 'genius-addon-lite'),
            'selectors' => [
                "{{WRAPPER}} .accordion-wrapper .card .card-header a[aria-expanded=true]" => "color: {{VALUE}}",
            ]
        ]);
        $this->add_control('tab_paragraph_color', [
            'type' => Controls_Manager::COLOR,
            'label' => esc_html__('Paragraph Color', 'genius-addon-lite'),
            'selectors' => [
                "{{WRAPPER}} .accordion-wrapper .card .card-body" => "color: {{VALUE}}",
            ]
        ]);
        $this->add_control('tab_icon_color', [
            'type' => Controls_Manager::COLOR,
            'label' => esc_html__('Icon Color', 'genius-addon-lite'),
            'selectors' => [
                "{{WRAPPER}} .accordion-wrapper .card .card-header a:after" => "color: {{VALUE}}",
            ]
        ]);
        $this->add_control('tab_icon_bg_color', [
            'type' => Controls_Manager::COLOR,
            'label' => esc_html__('Icon Background Color', 'genius-addon-lite'),
            'selectors' => [
                "{{WRAPPER}} .accordion-wrapper .card .card-header a:after" => "background-color: {{VALUE}}",
            ]
        ]);
        $this->add_control('tab_background', [
            'type' => Controls_Manager::COLOR,
            'label' => esc_html__('Background', 'genius-addon-lite'),
            'selectors' => [
                "{{WRAPPER}} .accordion-wrapper .card .card-body" => "background-color: {{VALUE}}",
                "{{WRAPPER}} .accordion-wrapper .card .card-header a[aria-expanded=true]" => "background-color: {{VALUE}}",
            ]
        ]);

        $this->end_controls_tab();

        $this->start_controls_tab(
            'button_style_hover_tab',
            [
                'label' => esc_html__('Normal', 'genius-addon-lite'),
            ]
        );

        $this->add_control('tab_hover_color', [
            'type' => Controls_Manager::COLOR,
            'label' => esc_html__('Title Color', 'genius-addon-lite'),
            'selectors' => [
                "{{WRAPPER}} .accordion-wrapper .card .card-header a" => "color: {{VALUE}}",
            ]
        ]);
        $this->add_control('tab_hover_paragraph_color', [
            'type' => Controls_Manager::COLOR,
            'label' => esc_html__('Paragraph Color', 'genius-addon-lite'),
            'selectors' => [
                "{{WRAPPER}} .accordion-wrapper .card .card-body" => "color: {{VALUE}}",
            ]
        ]);
        $this->add_control('tab_hover_icon_color', [
            'type' => Controls_Manager::COLOR,
            'label' => esc_html__('Icon Color', 'genius-addon-lite'),
            'selectors' => [
                "{{WRAPPER}} .accordion-wrapper .card .card-header a[aria-expanded=false]:after" => "color: {{VALUE}}",
            ]
        ]);
        $this->add_control('tab_hover_icon_bg_color', [
            'type' => Controls_Manager::COLOR,
            'label' => esc_html__('Icon Background Color', 'genius-addon-lite'),
            'selectors' => [
                "{{WRAPPER}} .accordion-wrapper .card .card-header a[aria-expanded=false]:after" => "background-color: {{VALUE}}",
            ]
        ]);
        $this->add_control('tab_hover_background', [
            'type' => Controls_Manager::COLOR,
            'label' => esc_html__('Background', 'genius-addon-lite'),
            'selectors' => [
                "{{WRAPPER}} .accordion-wrapper .card .card-body" => "background-color: {{VALUE}}",
                "{{WRAPPER}} .accordion-wrapper .card .card-header a" => "background-color: {{VALUE}}",
            ]
        ]);


        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();
        /*  tab styling tabs end */

        $this->start_controls_section(
            'typography_settings_section',
            [
                'label' => esc_html__('Typography Settings', 'genius-addon-lite'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_group_control(Group_Control_Typography::get_type(), [
            'label' => esc_html__('Title Typography', 'genius-addon-lite'),
            'name' => 'title_typography',
            'description' => esc_html__('select title typography', 'genius-addon-lite'),
            'selector' => "{{WRAPPER}} .accordion-wrapper .card .card-header a"
        ]);
        $this->add_group_control(Group_Control_Typography::get_type(), [
            'label' => esc_html__('Paragraph Typography', 'genius-addon-lite'),
            'name' => 'paragraph_typography',
            'description' => esc_html__('select Paragraph typography', 'genius-addon-lite'),
            'selector' => "{{WRAPPER}} .accordion-wrapper .card .card-body"
        ]);
        $this->end_controls_section();

    }

	/**
	 * Render Elementor widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
        $random_number = rand(3333,9999999);
        $accordion_items = $settings['accordion_items'];
		?>
        <div class="accordion-wrapper">
            <div id="xga-accordion-<?php echo esc_attr($random_number);?>">
                <?php
                $a = 0;
                foreach ( $accordion_items as $item ):
                    $is_open_initial =  esc_html($item['open']);
                    $is_open_heading_class = (!empty($is_open_initial)) ? 'open' : '';
                    $is_open_body_class = (!empty($is_open_initial)) ? 'show' : '';
                    $aria_expanded = (!empty($is_open_initial)) ? 'true' : 'false';

                    $a++;
                    ?>
                    <div class="xga-accrodion">
                        <div class="xga-accordion-heading">
                            <a href="" role="button"
                               class="xga-collapse <?php echo esc_attr($is_open_heading_class);?>"
                               aria-expanded="<?php echo esc_attr($aria_expanded);?>" >
                                <?php echo esc_html($item['title']);?>
                            </a>
                        </div>
                        <div class="xga-accordion-body <?php echo esc_attr($is_open_body_class);?>" data-parent="xga-accordion-<?php echo esc_attr($random_number); ?>">
                            <div class="xga-acc-content">
                                <?php echo esc_html($item['description']);?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
		<?php
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new Genius_Accordion_One() );