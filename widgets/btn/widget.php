<?php

namespace Elementor;
/**
 * Package genius Addon
 * Author Xgenious
 * @since 1.0.0
 * */

if ( ! defined( 'ABSPATH' ) ) {
	exit(); //exit if access directly
}

class Genius_Free_Button_One extends Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Elementor widget name.
	 *
	 * @return string Widget name.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_name() {
		return 'genius-free-btn-one-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Elementor widget title.
	 *
	 * @return string Widget title.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_title() {
		return esc_html__( 'Button', 'genius-addon-lite' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Elementor widget icon.
	 *
	 * @return string Widget icon.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_icon() {
		return 'eicon-button xga';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Elementor widget belongs to.
	 *
	 * @return array Widget categories.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_categories() {
		return [ 'ega-widgets' ];
	}

	/**
	 * Register Elementor widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'settings_section',
			[
				'label' => esc_html__( 'Button', 'genius-addon-lite' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);
		$this->add_control( 'html_selector', [
			'label'   => esc_html__( 'Selector', 'genius-addon-lite' ),
			'type'    => Controls_Manager::SELECT,
			'options' => [
				'a'      => esc_html__( 'a', 'genius-addon-lite' ),
				'button' => esc_html__( 'button', 'genius-addon-lite' )
			],
			'default' => 'a',
		] );
		$this->add_control( 'btn_text', [
			'label'     => esc_html__( 'Text', 'genius-addon-lite' ),
			'type'      => Controls_Manager::TEXT,
			'default'   => esc_html__( 'Button Text', 'genius-addon-lite' ),
			'separator' => 'before'
		] );
		$this->add_control( 'btn_url', [
			'label'     => esc_html__( 'URL', 'genius-addon-lite' ),
			'type'      => Controls_Manager::URL,
			'condition' => [
				'html_selector' => 'a'
			]
		] );
		if ( genius_version_compare( ELEMENTOR_VERSION, '2.6.0', '>=' ) ) {
			$this->add_control( 'icon', [
				'label'     => esc_html__( 'Icon', 'genius-addon-lite' ),
				'type'      => Controls_Manager::ICONS,
				'separator' => 'after'
			] );
		} else {
			$this->add_control( 'icon', [
				'label'     => esc_html__( 'Icon', 'genius-addon-lite' ),
				'type'      => Controls_Manager::ICON,
				'separator' => 'after'
			] );
		}
		$this->add_control( 'btn_size', [
			'label'   => esc_html__( 'Sizes', 'genius-addon-lite' ),
			'type'    => Controls_Manager::SELECT,
			'options' => [
				'ega-btn-lg' => esc_html__( 'Large', 'genius-addon-lite' ),
				'ega-btn-md' => esc_html__( 'Medium', 'genius-addon-lite' ),
				'ega-btn-sm' => esc_html__( 'Small', 'genius-addon-lite' ),
			],
			'default' => 'ega-btn-lg',

		] );


		if ( genius_version_compare( ELEMENTOR_VERSION, '2.6.0', '>=' ) ) {
			$icon_condition = [ 'icon[value]!' => '' ];
		} else {
			$icon_condition = [ 'icon!' => '' ];
		}
		$this->add_control( 'icon_position', [
			'label'     => esc_html__( 'Icon Position', 'genius-addon-lite' ),
			'type'      => Controls_Manager::CHOOSE,
			'options'   => [
				'left'   => [
					'icon'  => 'fa fa-arrow-left',
					'title' => esc_html__( 'Left', 'genius-addon-lite' ),
				],
				'top'    => [
					'icon'  => 'fa fa-arrow-up',
					'title' => esc_html__( 'Top', 'genius-addon-lite' ),
				],
				'bottom' => [
					'icon'  => 'fa fa-arrow-down',
					'title' => esc_html__( 'Bottom', 'genius-addon-lite' ),
				],
				'right'  => [
					'icon'  => 'fa fa-arrow-right',
					'title' => esc_html__( 'Right', 'genius-addon-lite' ),
				],
			],
			'default'   => 'left',
			'condition' => $icon_condition

		] );
		$this->add_control( 'icon_gap', [
			'label'      => esc_html__( 'Icon Gap', 'genius-addon-lite' ),
			'type'       => Controls_Manager::SLIDER,
			'size_units' => [ 'px', '%' ],
			'range'      => [
				'px' => [
					'min'  => 0,
					'step' => 1,
					'max'  => 100
				],
				'%'  => [
					'min'  => 0,
					'step' => 1,
					'max'  => 100
				]
			],
			'selectors'  => [
				"{{WRAPPER}} .ega-btn-wrapper.ega-btn-icon-position-top .ega-btn-icon"    => "padding-bottom: {{SIZE}}{{UNIT}}",
				"{{WRAPPER}} .ega-btn-wrapper.ega-btn-icon-position-bottom .ega-btn-icon" => "padding-top: {{SIZE}}{{UNIT}}",
				"{{WRAPPER}} .ega-btn-wrapper.ega-btn-icon-position-left .ega-btn-icon"   => "padding-right: {{SIZE}}{{UNIT}}",
				"{{WRAPPER}} .ega-btn-wrapper.ega-btn-icon-position-right .ega-btn-icon"  => "padding-left: {{SIZE}}{{UNIT}}"
			],
			'condition' => $icon_condition
		] );
		$this->add_control( 'btn-alignment', [
			'label'     => esc_html__( 'Alignment', 'genius-addon-lite' ),
			'type'      => Controls_Manager::CHOOSE,
			'options'   => [
				'left'   => [
					'icon'  => 'fa fa-align-left',
					'title' => esc_html__( 'Left', 'genius-addon-lite' )
				],
				'center' => [
					'icon'  => 'fa fa-align-center',
					'title' => esc_html__( 'Center', 'genius-addon-lite' )
				],
				'right'  => [
					'icon'  => 'fa fa-align-right',
					'title' => esc_html__( 'Right', 'genius-addon-lite' )
				]
			],
			'default'   => 'left',
			'selectors' => [
				"{{WRAPPER}} .ega-btn-wrapper" => "text-align: {{VALUE}}"
			]
		] );
		$this->end_controls_section();

		$this->start_controls_section( 'btn-styling', [
			'label' => esc_html__( 'Button Style', 'genius-addon-lite' ),
			'tab'   => Controls_Manager::TAB_STYLE
		] );

		$this->start_controls_tabs( 'btn_styling_tabs' );

		$this->start_controls_tab( 'btn_normal', [
			'label' => esc_html__( 'Normal', 'genius-addon-lite' )
		] );
		$this->add_control( 'btn_color', [
			'label'     => esc_html__( 'Color', 'genius-addon-lite' ),
			'type'      => Controls_Manager::COLOR,
			'default'   => '#fff',
			'selectors' => [
				"{{WRAPPER}} .ega-btn-wrapper .ega-btn .ega-btn-text" => "color: {{VALUE}}"
			]
		] );
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'     => 'btn-text-background',
				'label'    => esc_html__( 'Background', 'genius-addon-lite' ),
				'types'    => [ 'classic', 'gradient', 'video' ],
				'selector' => "{{WRAPPER}} .ega-btn-wrapper .ega-btn"
			] );
		$this->add_responsive_control( 'btn-padding', [
			'label'      => esc_html__( 'Padding', 'genius-addon-lite' ),
			'type'       => Controls_Manager::DIMENSIONS,
			'size_units' => [ 'px', 'em' ],
			'selectors'  => [
				"{{WRAPPER}} .ega-btn-wrapper .ega-btn" => "padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;"
			]
		] );
		$this->add_group_control(
			Group_Control_Typography::get_type()
			, [
			'label' => esc_html__( 'Typography', 'genius-addon-lite' ),
			'name'  => 'btn-text-typography',
				'selector' => "{{WRAPPER}} .ega-btn-wrapper .ega-btn"
		] );
		$this->add_group_control(
			Group_Control_Border::get_type(),[
				'label' => esc_html__('Border','genius-addon-lite'),
				'name' => 'btn-text-border',
				'selector' => "{{WRAPPER}} .ega-btn-wrapper .ega-btn"
		]);
		$this->add_responsive_control( 'btn-border-radius', [
			'label'      => esc_html__( 'Border Radius', 'genius-addon-lite' ),
			'type'       => Controls_Manager::DIMENSIONS,
			'size_units' => [ 'px' ],
			'selectors'  => [
				"{{WRAPPER}} .ega-btn-wrapper .ega-btn" => "border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}"
			]
		] );
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),[
			'label' => esc_html__('Box Shadow','genius-addon-lite'),
			'name' => 'btn-text-box-shadow',
			'selector' => "{{WRAPPER}} .ega-btn-wrapper .ega-btn"
		]);

		$this->end_controls_tab();
		$this->start_controls_tab( 'btn_hover', [
			'label' => esc_html__( 'Hover', 'genius-addon-lite' )
		] );
		$this->add_control( 'btn_hover_color', [
			'label'     => esc_html__( 'Color', 'genius-addon-lite' ),
			'type'      => Controls_Manager::COLOR,
			'default'   => '#fff',
			'selectors' => [
				"{{WRAPPER}} .ega-btn-wrapper .ega-btn:hover .ega-btn-text" => "color: {{VALUE}}"
			]
		] );
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'     => 'btn-hover-background',
				'label'    => esc_html__( 'Background', 'genius-addon-lite' ),
				'types'    => [ 'classic', 'gradient', 'video' ],
				'selector' => "{{WRAPPER}} .ega-btn-wrapper .ega-btn:hover"
			] );
		$this->add_group_control(
			Group_Control_Border::get_type(),[
			'label' => esc_html__('Border','genius-addon-lite'),
			'name' => 'btn-hover-border',
			'selector' => "{{WRAPPER}} .ega-btn-wrapper .ega-btn:hover"
		]);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),[
			'label' => esc_html__('Box Shadow','genius-addon-lite'),
			'name' => 'btn-hover-box-shadow',
			'selector' => "{{WRAPPER}} .ega-btn-wrapper .ega-btn:hover"
		]);
		$this->end_controls_tab();
		$this->end_controls_tabs();
		$this->end_controls_section();

		$this->start_controls_section( 'icon-styling', [
			'label' => esc_html__( 'Icon Style', 'genius-addon-lite' ),
			'tab'   => Controls_Manager::TAB_STYLE
		] );

		$this->start_controls_tabs('icon-style-tabs');
		$this->start_controls_tab('icon-normal',[
			'label' => esc_html__('Normal','genius-addon-lite')
		]);
		$this->add_control('icon-color',[
			'label' => esc_html__('Color','genius-addon-lite'),
			'type' => Controls_Manager::COLOR,
			'default' => '#fff',
			'selectors' => [
				"{{WRAPPER}} .ega-btn-wrapper .ega-btn .ega-btn-icon" => "color: {{VALUE}}"
			]
		]);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'     => 'btn-icon-background',
				'label'    => esc_html__( 'Background', 'genius-addon-lite' ),
				'types'    => [ 'classic', 'gradient', 'video' ],
				'selector' => "{{WRAPPER}} .ega-btn-wrapper .ega-btn .ega-btn-icon"
			] );
		$this->add_responsive_control( 'btn-icon-padding', [
			'label'      => esc_html__( 'Padding', 'genius-addon-lite' ),
			'type'       => Controls_Manager::DIMENSIONS,
			'size_units' => [ 'px', 'em' ],
			'selectors'  => [
				"{{WRAPPER}} .ega-btn-wrapper .ega-btn .ega-btn-icon" => "padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;"
			]
		] );
		$this->add_group_control(
			Group_Control_Border::get_type(),[
			'label' => esc_html__('Border','genius-addon-lite'),
			'name' => 'btn-icon-border',
			'selector' => "{{WRAPPER}} .ega-btn-wrapper .ega-btn .ega-btn-icon"
		]);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),[
			'label' => esc_html__('Box Shadow','genius-addon-lite'),
			'name' => 'btn-icon-box-shadow',
			'selector' => "{{WRAPPER}} .ega-btn-wrapper .ega-btn .ega-btn-icon"
		]);
		$this->end_controls_tab();
		$this->start_controls_tab('icon-hover',[
			'label' => esc_html__('Hover','genius-addon-lite')
		]);
		$this->add_control('icon-hover-color',[
			'label' => esc_html__('Color','genius-addon-lite'),
			'type' => Controls_Manager::COLOR,
			'default' => '#fff',
			'selectors' => [
				"{{WRAPPER}} .ega-btn-wrapper .ega-btn:hover .ega-btn-icon" => "color: {{VALUE}}"
			]
		]);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'     => 'btn-icon-hover-background',
				'label'    => esc_html__( 'Background', 'genius-addon-lite' ),
				'types'    => [ 'classic', 'gradient', 'video' ],
				'selector' => "{{WRAPPER}} .ega-btn-wrapper .ega-btn:hover .ega-btn-icon"
			] );
		$this->add_group_control(
			Group_Control_Border::get_type(),[
			'label' => esc_html__('Border','genius-addon-lite'),
			'name' => 'btn-icon-hover-border',
			'selector' => "{{WRAPPER}} .ega-btn-wrapper .ega-btn:hover .ega-btn-icon"
		]);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),[
			'label' => esc_html__('Box Shadow','genius-addon-lite'),
			'name' => 'btn-icon-hover-box-shadow',
			'selector' => "{{WRAPPER}} .ega-btn-wrapper .ega-btn:hover .ega-btn-icon"
		]);
		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	/**
	 * Render Elementor widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
		$this->add_render_attribute( 'ega-btn-wrapper', 'class', 'ega-btn-wrapper' );
		$this->add_render_attribute( 'ega-btn-wrapper', 'class', 'ega-btn-icon-position-' . $settings['icon_position'] );
		$this->add_render_attribute( 'ega-btn', 'class', 'ega-btn' );
		$this->add_render_attribute( 'ega-btn', 'class', $settings['btn_size'] );

		if ( ! empty( $settings['btn_url']['url'] ) && 'a' === $settings['html_selector'] ) {
			$this->add_render_attribute( 'ega-btn', 'href', $settings['btn_url']['url'] );
		}
		if ( ! empty( $settings['btn_url']['is_external'] ) ) {
			$this->add_render_attribute( 'ega-btn', 'target', '_blank' );
		}
		if ( ! empty( $settings['btn_url']['nofollow'] ) ) {
			$this->add_render_attribute( 'ega-btn', 'rel', 'nofollow' );
		}
		if ( genius_version_compare( ELEMENTOR_VERSION, '2.6.0', '>=' ) ) {
			$icon = ! empty( $settings['icon']['value'] ) ? '<span class="ega-btn-icon">' . Genius_Icon_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] ) . '</span>' : '';
		} else {
			$icon = ! empty( $settings['icon'] ) ? sprintf( '<span class="ega-btn-icon">%1$s</span>', $settings['icon'] ) : '';
		}
		$btn_text = sprintf( '<span class="ega-btn-text">%1$s</span>', $settings['btn_text'] );

		printf( '<div %1$s><%5$s %2$s>%3$s %4$s</%5$s></div>',
			$this->get_render_attribute_string( 'ega-btn-wrapper' ),
			$this->get_render_attribute_string( 'ega-btn' ),
			$icon,
			$btn_text,
			$settings['html_selector']
		);
		?>
		<?php
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new Genius_Free_Button_One() );