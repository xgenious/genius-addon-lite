<?php
namespace Elementor;
/**
 * Package genius Addon
 * Author Xgenious
 * @since 1.0.0
 * */

if (!defined('ABSPATH')){
	exit(); //exit if access directly
}
class Genius_Logo_One extends Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Elementor widget name.
	 *
	 * @return string Widget name.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_name() {
		return 'genius-logo-one-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Elementor widget title.
	 *
	 * @return string Widget title.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_title() {
		return esc_html__( 'Logo Slider', 'genius-addon-lite' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Elementor widget icon.
	 *
	 * @return string Widget icon.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_icon() {
		return 'eicon-logo xga';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Elementor widget belongs to.
	 *
	 * @return array Widget categories.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_categories() {
		return [ 'ega-widgets' ];
	}

	/**
	 * Register Elementor widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls()
	{

		$this->start_controls_section(
			'settings_section',
			[
				'label' => esc_html__('General Settings', 'genius-addon-lite'),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);
		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'image', [
				'label' => esc_html__('Image', 'genius-addon-lite'),
				'type' => Controls_Manager::MEDIA,
				'show_label' => false,
			]
		);
        $repeater->add_control(
            'tooltip_title',
            [
                'label' => esc_html__('Tooltip Title', 'genius-addon-lite'),
                'type' => Controls_Manager::TEXT,
                'description' => esc_html__('enter title.', 'genius-addon-lite'),
                'default' => esc_html__('Brand Name', 'genius-addon-lite'),
            ]
        );
		$this->add_control('screenshort_items', [
			'label' => esc_html__('Brand Slider Item', 'genius-addon-lite'),
			'type' => Controls_Manager::REPEATER,
			'fields' => $repeater->get_controls(),
			'default' => [
				[
					'image' => array(
						'url' => Utils::get_placeholder_image_src()
					)
				]
			],

		]);
		$this->end_controls_section();

		$this->start_controls_section(
			'slider_settings_section',
			[
				'label' => esc_html__('Slider Settings', 'genius-addon-lite'),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);
		$this->add_control(
			'items',
			[
				'label' => esc_html__('Items', 'genius-addon-lite'),
				'type' => Controls_Manager::TEXT,
				'description' => esc_html__('you can set how many item show in slider', 'genius-addon-lite'),
				'default' => '4'
			]
		);
		$this->add_control(
			'margin',
			[
				'label' => esc_html__('Margin', 'genius-addon-lite'),
				'description' => esc_html__('you can set margin for slider', 'genius-addon-lite'),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
						'step' => 5,
					]
				],
				'default' => [
					'unit' => 'px',
					'size' => 30,
				],
				'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .brand-carousel-wrapper .slick-list .slick-slide' => 'margin: {{SIZE}}{{UNIT}};',
                ],
			]
		);
		$this->add_control(
			'loop',
			[
				'label' => esc_html__('Loop', 'genius-addon-lite'),
				'type' => Controls_Manager::SWITCHER,
				'description' => esc_html__('you can set yes/no to enable/disable', 'genius-addon-lite')
			]
		);
		$this->add_control(
			'autoplay',
			[
				'label' => esc_html__('Autoplay', 'genius-addon-lite'),
				'type' => Controls_Manager::SWITCHER,
				'description' => esc_html__('you can set yes/no to enable/disable', 'genius-addon-lite'),

			]
		);
		$this->add_control(
			'autoplaytimeout',
			[
				'label' => esc_html__('Autoplay Timeout', 'genius-addon-lite'),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 10000,
						'step' => 2,
					]
				],
				'default' => [
					'unit' => 'px',
					'size' => 5000,
				],
				'size_units' => ['px'],
				'condition' => array(
					'autoplay' => 'yes'
				)
			]

		);
		$this->end_controls_section();

	}

	/**
	 * Render Elementor widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings_for_display();
		$all_screenshort_item = $settings['screenshort_items'];
		$rand_numb = rand(333, 999999999);

		//slider settings
		$slider_settings = [
			"loop" => esc_attr($settings['loop']),
			"items" => esc_attr($settings['items'] ?? 1),
			"autoplay" => esc_attr($settings['autoplay']),
			"autoplaytimeout" => esc_attr($settings['autoplaytimeout']['size'] ?? 0),
		]
		//slick-carousel
		?>
		<div class="brand-carousel-wrapper">
			<div class="brands-carousel"
				 id="brand-one-carousel-<?php echo esc_attr($rand_numb); ?>"
				 data-settings='<?php echo json_encode($slider_settings) ?>'
			>
				<?php
				foreach ($all_screenshort_item as $item):
					$image_id = $item['image']['id'] ?? '' ;
					$image_url = !empty($image_id) ? wp_get_attachment_image_src($image_id, 'full', false)[0] : '';
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
					?>
					<div class="single-brand-item" <?php if (!empty($item['tooltip_title'])){printf('title="%1$s"',$item['tooltip_title']);}?>>
						<img src="<?php echo esc_url($image_url); ?>" alt="<?php echo esc_attr($image_alt); ?>">
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		<?php
	}
}
Plugin::instance()->widgets_manager->register_widget_type( new Genius_Logo_One() );