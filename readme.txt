=== Genius Addons For Elementor ===
Contributors: Xgenious
Tags: elementor addons, elementor widgets, elementor plugins, mega menu, mega menu builder, header footer builder, elements, elementor extensions, elementor modules, page builder addons, addons, essential plugins, essential widgets, elemntor pro, free addons, free widgets, free plugins
Requires at least: 5.0
Tested up to: 5.8
Stable tag: 1.0.0
Requires PHP: 7.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Genius Addon comes up with ultimate Elementor blocks and widgets. Ready section and flexible option makes it more efficient for the users.

== Description ==

Genius Addon – Elementor Addons comes up with ultimate Elementor blocks and widgets. Template Library, Ready section and flexible option makes it more efficient for the users. 71+ Elementor Addons & Elementor widgets will help you to save time and effort. Elementor Mega menu, Header & Footer builder makes things easy for create limitless websites with number of features. Controls over the Addons and premade designed sections make it more powerful. These addons are designed and decorated with creativity and usability. Genius Addon –  Elementor Addons will make sure that ,you get all the addons or plugins under a single roof to keep you away from multiple plugins uses.

<iframe width="560" height="315" src="https://www.youtube.com/embed/h0ePGHmUH3c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[ **EXPLORE OUR DEMOS** ](https://geniusaddon.com/)

**FOR SUPPORT & ADVICES** : [Open a ticket](https://geniusaddon.com/contact-us) OR [Send us emails](mailto:geniusaddon@gmail.com)
To know about UPCOMING Widgets & Features: [ HERE ](https://geniusaddon.com/)
**FOR DOCUMENTATION** : [View Documentation](https://geniusaddon.com/docs/genius-addon/)

Ultimate Elementor addon blocks and widgets with exclusive features and options will surely a time saving kit for you. Mega menu, Header & Footer Builder, 3rd party extensions like WooCommerce, Easy Digital Downloads and LearnPress ( learning management system) plugins gives you the control to build sites more easily. Number of ready blocks will give you a new experience in building website with  Page Builder.

Genius Addon Addons Is build by [Xgenious](https://themeforest.net/user/xgenious/portfolio) and we have 10000+ WordPress Themes & plugins Users from different parts of the world.

you can explore all the features and get experience of Genius Addon website.


==== NOTE: ====
[ELEMENTOR](https://wordpress.org/plugins/elementor/) PAGE BUILDER IS REQUIRED FOR THIS PLUGIN.

==== KEY FEATURES ====
- Mega Menu Builder.
- Unlimited Icons & Sets.
- Free and Premium elements.
- 13+ Widgets.
- Widgets Accordion
- Widgets Alert Block.
- Widgets Blockquote.
- Widgets Button.
- Widgets Card.
- Widgets Contact Form 7.
- Widgets Icon Box.
- Widgets Info Box.
- Widgets Image Hover.
- Widgets Logo Slider.
- Widgets Navigation.
- Widgets Tabs.
- Widgets Team Slider.
- **Upcoming More...**


== Screenshots ==

== Changelog ==

= 1.0.0 =
* Initial release

== Upgrade Notice ==

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. All Settings will be found in Admin sidebar -> Genius Addon


== Frequently Asked Questions ==

= Is this compatible Elementor Pro? =
Yes it's compatible with Elementor Pro.

= How to use Genius Addon? =
Login your WordPress Dashboard, From the left menu click Genius Addon.

= Elementor editor fails to load or not working? =
It's due to your server PHP settings. Increase your server PHP memory limit from the wp-config.php file or php.ini file. If you don't have an idea about it. Please contact your hosting provider and ask to increase
* PHP memory_limit = 512M
* max_execution_time = 300

= Do you planed for regular update? =
Yes we have full planed for continuously update.

= Have a pro a plugin? =
Yes we have pro version [get the pro version](https://geniusaddon.com/)

= Credit =
Line Awesome [https://icons8.com/line-awesome]
Slick Slider [https://kenwheeler.github.io/slick/]